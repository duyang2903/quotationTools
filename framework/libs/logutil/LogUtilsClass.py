# !/usr/bin/env python3
#encode:UTF-8
# 说明：提示信息类

from framework.libs.config.configParserClass   import *
# 信息信息打印级别
DEBUG = 0;
INFO = 1;
ERROR = 2;
NOTHING =3 ;
# level = DEBUG;	
level=int(getParser("loglevel","level"))
# level = INFO
	
def debug(msg):
	if (DEBUG >= level):
		print(msg);

def info (msg):
	if(INFO >= level):
		print(msg);

def error (msg):
	if (ERROR >= level):
		print(msg);

if __name__ == '__main__':
	msg = "debug";
	debug(msg);
		