# !/usr/bin/env python3
#encode:UTF-8
# 说明：xlr读取类

import xlrd
import sys

class XlsReader (object):
	def getList(self,excelPathName , sheetName):
		pass;
	def getAssociativeArray (self, excelPathName, sheetName , inputHeaderKey):	
		pass;

class XlrdTool(XlsReader):
	# 作用：获取数组
	def getList (self,excelPathName,sheetName):
		try :
			sheetList = xlrd.open_workbook(excelPathName).sheet_by_name(sheetName);
			list = [sheetList.row_values(i) for i in range(0,sheetList.nrows)];
			return list;
		except Exception as data:
			print("Can't Open the excel, %s" % data);
		
	# 作用：获取关联数组
	# inputHeaderKey：数组每一列的对应的键值
	# 返回：一个数组，数组的每一行为一个dict，代表原来表格里面的每一行，其中此dict的键名为输入的inputHeaderKey，键值为读入的excel文件的对应值。
	
	def getAssociativeArray (self, excelPathName, sheetName , inputHeaderKey):
		list = []
		try:
			sheetList = xlrd.open_workbook(excelPathName).sheet_by_name(sheetName);
			# if len(inputHeaderKey) != sheetList.ncols:
				# print ("输入的键值与列数不匹配");
				# sys.exit();
			# ***********方法一************
			# for row in range(0, sheetList.nrows):
				# rowDict = {};
				# for col in range(len(inputHeaderKey)):
					# rowDict[inputHeaderKey[col]] = sheetList.row_values(row)[col];

				# list.append(rowDict);
				# *************方法二************
			# for row in range(0, sheetList.nrows):
				# # 取出每一行的数据
				# rowValues = sheetList.row_values(row);
				# # 将每一行数据转换为字典形式，然后组装成数组
				# # zip将将对象中对应的元素打包成一个个tuple（元组），然后使用dict转换为字典
				# rowDict = dict(zip (inputHeaderKey , rowValues));
				# list.append(rowDict);
				# ************方法三**********
			# row：表示从当前sheet读出了的每一行，
			# 将每一行的row_values与inputHeaderKey组成dict
			list = [dict (zip (inputHeaderKey , sheetList
			.row_values(row))) for row in range(sheetList.nrows)];
			
		except Exception as data:
			print("打开文件失败,%s" % data);
		return list;
 		
		
if __name__ == '__main__':
	excelPathName = "D:\python\pythonProject\quotationTools\project\招银云创服务器零散采购7_20170717.xls";
	sheetName = "价格明细清单";
	xlrdTool = XlrdTool();
	# print(xlrd.getList());
	# list = xlrdTool.getList(excelPathName , sheetName);
	inputHeaderKey = ['ID','BOM','typeID','description','num','listprice','off','price','totalPrice','totalListPrice','productLine','waston','addOn'];
	list = xlrdTool.getAssociativeArray(excelPathName,sheetName,inputHeaderKey);
	print(list)
	
