# !/usr/bin/env python3
#encode:UTF-8
# 说明：封装了对MySQL数据库的基本操作
import mysql.connector
from framework.libs.logutil.LogUtilsClass      import *

class MySQL(object):
	# 单例模式
	__instance = None
	def __init__(self):
		pass

	def __new__(cls,*args,**kwargs):
		if not cls.__instance:
			cls.__instance = super(MySQL,cls).__new__(cls,*args,**kwargs);
		return cls.__instance;
		return rs;
	# 连接
	# config：dict类型，相关的配置
	def connect(self,config):
		conn = False
		try:   
			self.conn = mysql.connector.connect(**config);
			info("使用数据库作为数据源")
		except Exception as data:
			self.conn = False
			error("数据库连接失败" )
			info("使用原始表格作为数据源")
	# 关闭数据库
	def close(self):
		if(self.conn):
			try:
				if(type(self.cursor)=='object'):
					self.cursor.close();
				if(type(self.cursor)=='object'):
					self.conn.close();
			except Exception as data:
				error('关闭数据库出现错误')
	
	# 查询
	def findOne(self,sql):
		rs = "";
		if (self.conn):
			try:
				# 获得游标
				self.cursor = self.conn.cursor()
				# 执行SQL语句
				self.cursor.execute(sql)
				# rs是返回的结果
				rs = self.cursor.fetchall()
			except Exception as data :
				rs = False
				error("查询出现错误, %s" % data)
		return  rs;
	# 插入记录			
	# table：表名
	# arr：一个字典,key是字段名称，value是需要插入的值
	def insert(self,table , arr):
	# $sql = "insert into 表名(多个字段) values(多个值)";
		keyArr = [];
		valueArr = [];
		for key,value in arr.items():
			value = str(value).lstrip().rstrip();
			keyArr.append("`"+key+"`");
			valueArr.append("'"+value+"'");
		
		keys = ",".join(keyArr);
		values = ",".join(valueArr);
		# 组装SQL
		sql = "insert into " + table + " (" + keys + ") values ("  + values + ")";
		debug(sql)
		if (self.conn):
			try:
				self.cursor = self.conn.cursor()
				# 执行SQL语句
				self.cursor.execute(sql)
				# 提交
				self.conn.commit();

			except Exception as data :
				error("插入出现错误, %s" % data)
				
	# **************更新表格**************
	def updateTable(self,table , arr , where)	:
		# //update 表名 set 字段=字段值 where ……
		keyAndvalueArr = [];
		for key,value in arr.items():
			value = str(value).lstrip().rstrip();
			keyAndvalueArr.append( "`" + key + "`='"  + value + "'");
			
		keyAndvalues  = ",".join(keyAndvalueArr);
		# 组装SQL
		sql = "update " + table + " set " + keyAndvalues + " where " + where ; 
		sql = "insert into " + table + " (" + key + ") values ("  + value + ")";#修改操作 格式 update 表名 set 字段=值 where 条件
		debug(sql)		
		if (self.conn):
			try:
				self.cursor = self.conn.cursor()
				self.cursor.execute(sql)
				# 提交
				self.conn.commit();
			except Exception as data :
				error("更新出现错误, %s" % data)		
	
	
# 测试
if __name__ == "__main__":
	dbTools = MySQL();
	config = {
	"host":"127.0.0.1",
	"port":"3306",
	"db":"quotation",
	"user":"root",
	"passwd":"root",
	"charset":"utf8"
};
	dbTools.connect(config);
	# sql ="SELECT *  from h3c where BOM = '9801A0W4' ";
	# sql ="SELECT `typeID`,`BOM`  from h3c where BOM = '' ";
	# sql = "SELECT `typeID`,`BOM`  from h3c where BOM = '9801A0W4' ";
	# sql = "select COLUMN_NAME from information_schema.COLUMNS where table_name = 'h3c';"
	# rs = dbTools.findOne(sql);
	# print(rs)
	arr = {"BOM":"9801A0W4","typeID":"duyang"}
	# dbTools.insert("h3c",arr);
	dbTools.updateTable("h3c",arr,"`BOM`='9801A0W4'");
	
	dbTools.close();
	