# !/usr/bin/env python3
#encode:UTF-8
# 说明：excel打印类

import xlsxwriter
from framework.libs.logutil.LogUtilsClass import *


class XlsWriter(object):
	# 初始化的时候就打开将要Excel
	# destFile包含全路径
	def __init__(self, destFile):
		self.workbook = xlsxwriter.Workbook(destFile);
	# 关闭
	def close(self):
		self.workbook.close();

	# outputKeys是outputParam的键值,是个数组
	# view :String类型
	def assign(self, lists, outputKeys, sheetName, view):
		self.lists = lists;
		self.outputKeys = outputKeys;
		self.sheetName = sheetName;
		self.view = view;
		# 从文件中读取format的类型，获得format数组
		self.dFormat = {};
		self.dFormat = self.addFormat();
		# 将要输出的outputKeys与每一列的序号组成dict，方便后面调度
		colOrdinal = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
		              'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
		# 先组合成为dict
		self.colIndexes = dict(zip(self.outputKeys, colOrdinal));
		
	# 展示类
	def display(self):
		# 每一行的格式由打印单元格的时候给出
		self.printList();
		# 设置每一列的宽度
		self.setColumn();
		# 设置筛选
		self.setAutofilter();
		# 冻结首行
		self.freezeTopRow();

	# ***************加载format*****************
	def addFormat(self):
		# 默认在tpl文件中设置每个sheet的格式
		tpl = __import__("tpl." + self.view)
		sheet = getattr(tpl , self.view);
		types = getattr(sheet , "types");
		typeDict = {};
		# # 对所列出来的颜色类型的名称一一的列举
		for type in types:
			typeDict [type] = self.workbook.add_format(getattr(sheet , type ));
		return typeDict;
		
	# **********打印数组，对每个单元格设置不同的格式
	def printList(self):
		sheetName = self.sheetName;
		# 如果sheetName对应的sheet还没建立则建立，若已建立则获取
		if self.workbook.get_worksheet_by_name(sheetName) is None:
			worksheet = self.workbook.add_worksheet(sheetName);
		else:
			worksheet = self.workbook.get_worksheet_by_name(sheetName);
		# 定义全局变量
		col = 0;
		arr = self.lists;
		# 依次取每一行
		for row in range(len(arr)):
			# 依次取每个要输出的每一列对应的key值
			for outkey in self.outputKeys:
				formatKey = "";
				# 若outkey等于discount,rate则单独设置格式
				if outkey == "discount" or outkey == 'rate':
					formatKey = arr[row]["colorTag"] + "Off";
				else :
					formatKey = arr[row]["colorTag"];
				
				# 打印每个单元格
				worksheet.write(row , col , arr[row][outkey] , self.dFormat[formatKey])
				col += 1;

			col = 0;
		return worksheet;


	# *************设置每列的宽度*************
	def setColumn(self):
		# 列序号
		worksheet = self.workbook.get_worksheet_by_name(self.sheetName);
		tpl = __import__("tpl." + self.view)
		sheet = getattr(tpl , self.view);
		colBreath = getattr(sheet , "colbreath");
		for outputKey in self.outputKeys:
			worksheet.set_column(self.colIndexes[outputKey] + ":" + self.colIndexes[outputKey], colBreath[outputKey]);
	# *************设置哪些列进行隐藏*************
	def setHidden(self, hideCols):
		worksheet = self.workbook.get_worksheet_by_name(self.sheetName);
		debug(self.sheetName)
		debug(self.workbook)
		debug(worksheet);
		# 取出hideCols与outputKeys中重合的部分
		hideColumns = [i for i in hideCols if i in self.outputKeys];
		for hideCol in hideColumns:
			worksheet.set_column(self.colIndexes[hideCol] + ":" + self.colIndexes	[hideCol], None,None, {'hidden': 1});
			
	# *************设置自动筛选*************
	def setAutofilter(self):
		worksheet = self.workbook.get_worksheet_by_name(self.sheetName);
		begin = "A1";
		# 末尾的列序号是outputKeys的最后一位
		endColumn = self.colIndexes[self.outputKeys[-1]];
		worksheet.autofilter("A1:" + endColumn + str(len(self.lists)));
		
	# *************冻结首行*************
	def freezeTopRow(self):
		worksheet = self.workbook.get_worksheet_by_name(self.sheetName);
		worksheet.freeze_panes(1, 0)
		return
	
	# *************打印URL*************
	def writeURL(self):
		worksheet = self.workbook.get_worksheet_by_name(self.sheetName);
		arr = self.lists;
		for i in range(1, len(arr) - 1):
			cellIndex = self.colIndexes['description'] + str(i + 1);
			worksheet.write_url(cellIndex, arr[i]['url'], self.dFormat['link'],arr[i]['description']);


if __name__ == "__main__":
	outputKeys = ['ID', 'num', 'description'];
	destFile = "test.xls";
	xlswriter = XlsWriter(destFile);

	lists = [{'ID': 1, 'num': 2, 'colorTag': 'header', 'description': 'A1',
	         'descString': 'description'},
	        {'ID': 1, 'num': 2, 'colorTag': 'site', 'description': 'desciption',
	         'descString': 'A1'},
	         {'ID': 1, 'num': 2, 'colorTag': 'general', 'description': 'desciption',
	          'descString': 'A1'},
	         {'ID': 1, 'num': 2, 'colorTag': 'subtotal', 'description': 'desciption',
	          'descString': 'A1'},
	         {'ID': 1, 'num': 2, 'colorTag': 'total', 'description': 'desciption',
	          'descString': 'A1'}
	         ]
	types = ["header", "headerOff", "site", "siteOff", "subtotal", "subtotalOff", "total", "totalOff", "general", "generalOff", "link" ] ;
	try:
		xlswriter.assign(lists, outputKeys, 'test', "totalConfigformat")
		xlswriter.display();
		xlswriter.setHidden(['num']);
		xlswriter.writeURL();
		# xlswriter.assign(list, outputKeys, 'test1', "detailSheet");
		# xlswriter.display();
		# xlswriter.setColumn();
		# xlswriter.setHidden(['ID']);
	except Exception as data:
		error("文档已经打开，或者不存在")
		error(data);
	finally:
		xlswriter.close();
