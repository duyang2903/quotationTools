#!/usr/bin/env python3 
# -*-coding:utf-8-*-
import configparser
import os
# from framework.libs.logutil.LogUtilsClass      import *
# 单例模式
# 作用：解析configure.conf文件。
#获取config配置文件
# 配置文件格式
# [section]
# key=value
def getParser (section, key):
	try:
		config = configparser.ConfigParser();
		# # 路径：当前文件上两层
		# os.path.dirname(__file__):获取当前文件的路径
		path = os.path.join(os.path.dirname(os.path.dirname (os.path.dirname(os.path.dirname(__file__)))),'configure.conf') ;
		# 文件格式：UTF-8
		config.read(path , 'UTF-8');
		
		value = config.get(section,key)
	except Exception as data:
		print("configure文件中存在两个相同的section"+str(data))
	return value
	