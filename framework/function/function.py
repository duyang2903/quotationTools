# 作用：调用libs中的Model , Controller , View类的功能函数

# name：字符
# 作用：初始化Model
def M(name):
	libs = __import__('libs.Model.'+name+'ModelClass');#动态的导入
	model = getattr(libs , "Model");#通过反射一步一步获得模块testModelClass
	testModelClass= getattr(model , name+'ModelClass');
	testModel = getattr( testModelClass, name+"Model");#通过反射获得类testModel
	return testModel();
	
# 作用：初始化Controller
def C(name , method)	:
	libs = __import__('libs.Controller.'+name+'ControllerClass');#动态的导入
	controller = getattr(libs , "Controller");#通过反射一步一步获得模块testControllerClass
	testControllerClass= getattr(controller , name+'ControllerClass');
	testController = getattr( testControllerClass, name+"Controller");#通过反射获得类testController
	obj = testController();#实例化类
	mtd = getattr(testController , method);#调用testController类中的get()方法。
	mtd(obj);
	
# def V(name , method)	:
# 作用：初始化Viewer
def V(name):
	libs = __import__('libs.View.'+name+'ViewClass');#动态的导入
	view = getattr(libs , "View");#通过反射一步一步获得模块testViewClass
	testViewClass= getattr(view , name+'ViewClass');
	testView = getattr( testViewClass, name+"View");#通过反射获得类testView
	obj = testView();#实例化类
	return obj;	
	

