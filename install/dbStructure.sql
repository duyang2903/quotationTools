-- drop database if exists `quotation`;
-- create database `quotation`;
-- use `quotation`;
-- DROP TABLE  IF EXISTS `h3c`;
-- CREATE TABLE IF NOT EXISTS `h3c` (
  -- `ID` int(3) NOT NULL AUTO_INCREMENT,
  -- `BOM` varchar(40) DEFAULT NULL,
  -- `typeID` varchar(40) DEFAULT NULL,
  -- `description` varchar(255) DEFAULT NULL,
  -- `unitsNetListPrice` decimal(10,0) DEFAULT NULL,
  -- PRIMARY KEY (`ID`)
-- ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19986 ;

-- drop database if exists `quotation`;
-- create database `quotation`;
use `quotation`;
DROP TABLE  IF EXISTS `cmbc`;
CREATE TABLE IF NOT EXISTS `cmbc` (
  `ID` int(3) NOT NULL AUTO_INCREMENT,
  `BOM` varchar(40) DEFAULT NULL,
  `typeID` varchar(40) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `unitsNetListPrice` decimal(10,0) DEFAULT NULL,
  `unitsNetPrice` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;
