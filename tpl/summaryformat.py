header={'num_format':'#,##0','font_name':'Arial','font_size':10,'font_color':'#000000','bg_color':'#c0c0c0','bold':True,'bottom':0}
headerOff={'num_format':'#,##0','font_name':'Arial','font_size':10,'font_color':'#000000','bg_color':'#c0c0c0','bold':True,'bottom':0}
subtotal={'num_format':'#,##0','font_name':'Arial','font_size':10,'font_color':'#000000','bg_color':'#c0c0c0','bold':True,'bottom':1}
subtotalOff={'num_format':'0.00%','font_name':'Arial','font_size':10,'font_color':'#000000','bg_color':'#c0c0c0','bold':True,'bottom':1}
general={'num_format':'#,##0','font_name':'Arial','font_size':10,'font_color':'#000000','bold':False,'bottom':0}
generalOff={'num_format':'0.00%','font_name':'Arial','font_size':10,'font_color':'#000000','bold':False,'bottom':0}



link={'num_format':'0','font_name':'Arial','font_size':10,'font_color':'blue','bold':False,'bottom':0}
# colbreath={"ID":3,"quantity":5,"BOM":10,"typeID":18,"description":50,"totalQuantity":5,"unitsNetListPrice":9,"discount":10,"unitsNetPrice":9,"totalPrice":11,"totalListPrice":11,"PL":10,"waston":10,"remarks":13}

colbreath = {
"ID":3,
"description":50,
"typeID":30,
"quantity":10,
"unitsNetPrice":11,
"totalPrice":11,
"rate":10
};

types = ["header", "headerOff", "subtotal", "subtotalOff", "general", "generalOff", "link" ] ;