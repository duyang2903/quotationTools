# !/usr/bin/env python3
#encode:UTF8
from  framework.includeList import  *


header={'num_format':'#,##0','font_name':'Arial','font_size':10,'font_color':'#000000','bg_color':'#c0c0c0','bold':True,'bottom':0}
headerOff={'num_format':'#,##0','font_name':'Arial','font_size':10,'font_color':'#000000','bg_color':'#c0c0c0','bold':True,'bottom':0}
site={'num_format':'0','font_name':'Arial','font_size':10,'font_color':'#000000','bg_color':'#339966','bold':False,'bottom':0}
siteOff={'num_format':'0.00%','font_name':'Arial','font_size':10,'font_color':'#000000','bg_color':'#339966','bold':False,'bottom':0}

general={'num_format':'#,##0','font_name':'Arial','font_size':10,'font_color':'#000000','bold':False,'bottom':0}
generalOff={'num_format':'0.00%','font_name':'Arial','font_size':10,'font_color':'#000000','bold':False,'bottom':0}


link={'num_format':'0','font_name':'Arial','font_size':10,'font_color':'blue','bold':False,'bottom':0}

colbreath = {
	"ID":3,
	"typeID":18,
	"description":50,
	"totalQuantity":10,
	"remarks":10
};

types = ["header", "headerOff", "site", "siteOff", "general", "generalOff", "link" ] ;




