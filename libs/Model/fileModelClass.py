# !/usr/bin/env python3
# encoding:UTF8
# author：杜阳

# *************作用：获得输入文件的文件名**************
import os.path
from framework.libs.config.configParserClass import *;
from  framework.libs.logutil.LogUtilsClass import  *
from  framework.includeList import  *

class fileModel(object):
	# 获得文件名
	def getProjectName(self):
		# 在configure.conf文件中获取inputfilePath
		filePath = getParser('path','inputfilePath');
		# 若为空，则filePath是主目录
		if filePath == "":
			filePath = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
		else:	
		# 若不为空，则将主目录与之拼接
			filePath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))) ,filePath)
		debug("输入文件的路径为"+filePath)
		# 获取project目录下的Excel文件
		files = [file for file in os.listdir(filePath) if file.rpartition('.')[2] == 'xls' or file.rpartition('.')[2]=='xlsx'];
		if len(files) == 0:
			print("当前目录没有Excel文件");
			exit();
		# 如果只有一个文件则选中
		elif len(files) == 1:
			fileSelected = files[0];
		else:
			# 存放最大的后缀编号
			max  = -1;
			# 存放选中的文件名
			fileSelected = ""
			try:
				for file in files :
					# 去掉后缀，并且依照"-"分割为数组
					fileSplitList = file.rpartition('.')[0].split('_');
					# 此时如果后缀编号是8位，表明是从NHCT导出的，不选
					if len(fileSplitList[-1] ) == 8:
						max = -1;
					# 如果倒数第一位比max还大，则更新
					elif int(fileSplitList[-1]) > max:
						max = int(fileSplitList[-1]);
						fileSelected = file;
			except Exception as data:		
				error("没有获取到处理的excel"+str(data));
		return fileSelected;
			
if __name__ == "__main__":
	fileModel = fileModel();
	print(fileModel.getProjectName())