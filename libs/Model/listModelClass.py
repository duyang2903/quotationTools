# !/usr/bin/env python3
#encode:UTF8
# author：杜阳
# —————————————————————作用：清单明细（不含价格）————————————————————
from  framework.includeList import  *
import copy;
class listModel(object):
	# **************传递list**************
	def assign(self,lists,fields):
		self.lists = copy.deepcopy(lists);
		self.fields = fields;
		colOrdinal = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
		              'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
		# 先组合成为dict		
		self.dCol = dict(zip(fields, colOrdinal));	
		self.getSubtotalIndex();
	
	# **************主函数：去掉价格**************
	def getPricefree(self):
		# 重构数量列
		self.rehandleQty();
		# 重构site行
		self.rehandleSite();
		# 去掉小计行
		for arr in self.lists[::-1]:
			if arr['colorTag'] == "subtotal" or arr['colorTag'] == 'total':
				self.lists.remove(arr);
		
		return self.lists;
	# **************获得索引表**************
	def getSubtotalIndex (self):
		self.aSite = [];
		self.aSubtotal = [];
		self.aTotal = 0;
		for i , arr in enumerate (self.lists):
			if arr['colorTag'] == 'site':
				self.aSite.append(i);
			elif arr['colorTag'] == 'subtotal':

				self.aSubtotal.append(i);
			elif arr['colorTag'] == 'total':
				self.aTotal = i;
			else:
				continue;	
	# **************重构site行**************
	def rehandleSite (self)	:
		for i , s in enumerate(self.aSite):
			if 'BOM' not in self.fields and  'BOM'  in self.lists[0].keys():
				self.lists[s]['typeID'] = self.lists[s]['BOM'];	
	# **************重构数量列**************
	def rehandleQty (self):
		try:
			if 'quantity' in self.fields :
				for i , s in enumerate(self.aSite):
					siteInitial = str(s + 1); 
					# siteEnd = self.aSubtotal[i]-1;
					for j in range(s + 1 , self.aSubtotal[i]-1 + 1):
						self.lists[j]['totalQuantity'] = '=$' + self.dCol['quantity'] + "$" + siteInitial + "*" + self.dCol['quantity'] + str (j + 1);
			elif 'quantity' in self.lists[0].keys():
				for i , s in enumerate(self.aSite):
					for j in range(s + 1 , self.aSubtotal[i]-1 + 1):
						self.lists[j]['totalQuantity'] = self.lists[s]['quantity'] * self.lists[j]['quantity'];
		except Exception as data:
			error("缺少数量列" + str(data));

			
			
		
if __name__ == "__main__":
	listmodel =listModel();
	lists = [
	{'ID': 'ID', 'BOM': '产品编码', 'typeID': '产品型号', 'description': '项目名称', 'quantity': '单套数量', 'totalQuantity': '总数量', 'unitsNetListPrice': '目录价', 'discount': '折扣', 'unitsNetPrice': '单价', 'totalPrice': '总价', 'remarks': '备注', 'colorTag': 'header'},
	 {'ID': 1, 'BOM': 'HP DL380 Gen9 24SFF CTO Server [#1]', 'typeID': '', 'description': '', 'quantity': 1.0, 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '=H25', 'unitsNetPrice': '', 'totalPrice': '', 'remarks': '', 'colorTag': 'site'},
	 {'ID': '', 'BOM': '767032-B21', 'typeID': 'ISS', 'description': 'HPE DL380 Gen9 24SFF CTO Server', 'quantity': 1.0, 'totalQuantity': '=$E$2*E3', 'unitsNetListPrice': 15100.0, 'discount': '=H2', 'unitsNetPrice': '=G3*H3', 'totalPrice': '=I3*F3', 'remarks': '', 'colorTag': 'general'},
	 {'ID': '', 'BOM': '767032-B21', 'typeID': 'ISS', 'description': 'HPE DL380 Gen9 24SFF CTO Server', 'quantity': 1.0, 'totalQuantity': '=$E$2*E3', 'unitsNetListPrice': 15100.0, 'discount': '=H2', 'unitsNetPrice': '=G3*H3', 'totalPrice': '=I3*F3', 'remarks': '', 'colorTag': 'general'},
	 {'ID': '', 'BOM': '767032-B21', 'typeID': 'ISS', 'description': 'HPE DL380 Gen9 24SFF CTO Server', 'quantity': 1.0, 'totalQuantity': '=$E$2*E3', 'unitsNetListPrice': 15100.0, 'discount': '=H2', 'unitsNetPrice': '=G3*H3', 'totalPrice': '=I3*F3', 'remarks': '', 'colorTag': 'general'},
	 {'ID': '', 'BOM': '小计', 'typeID': '', 'description': '=C3', 'quantity': '', 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '', 'unitsNetPrice': '', 'totalPrice': '=SUM(J3:J19)', 'remarks': '', 'colorTag': 'subtotal'},
	 {'ID': 2, 'BOM': 'BTO_1', 'typeID': '', 'description': '', 'quantity': 1.0, 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '=H25', 'unitsNetPrice': '', 'totalPrice': '', 'remarks': '', 'colorTag': 'site'},
	 {'ID': '', 'BOM': '455883-B21', 'typeID': 'ISS', 'description': 'HPE BLc 10G SFP+ SR Transceiver', 'quantity': 4.0, 'totalQuantity': '=$E$21*E23', 'unitsNetListPrice': 2900.0, 'discount': '=H21', 'unitsNetPrice': '=G23*H23', 'totalPrice': '=I23*F23', 'remarks': '', 'colorTag': 'general'},
	 {'ID': '', 'BOM': '小计', 'typeID': '', 'description': '=C22', 'quantity': '', 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '', 'unitsNetPrice': '', 'totalPrice': '=SUM(J22:J23)', 'remarks': '', 'colorTag': 'subtotal'},
	 {'ID': '', 'BOM': '总计', 'typeID': '', 'description': '', 'quantity': '', 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': 1, 'unitsNetPrice': '', 'totalPrice': '=SUM(J2:J24)/2', 'remarks': '', 'colorTag': 'total'}];
	dOutput = {
	"ID":"ID",
	"BOM":"产品编码",
	"typeID":"产品型号",
	"description":"项目名称",
	"quantity":"单套数量",
	"totalQuantity":"总数量",
	"unitsNetListPrice":"目录价",
	"discount":"折扣",
	"unitsNetPrice":"单价",
	"totalPrice":"总价",
	"totalListPrice":"总目录价",
	"remarks":"备注",
} ;
	# fields = {
		# "ID":"序号",
		# "description":"产品型号",
		# "typeID":"产品编码",
		# "quantity":"数量",
		# "unitsNetPrice":"单价",
		# "totalPrice":"总价",
		# "rate":"占比"
# };

	fields = {
	"ID":"序号",
	"description":"产品型号",
	"typeID":"产品编码",
	# "quantity":"数量",
	"totalQuantity":"数量",
	"unitsNetPrice":"单价",
	"totalPrice":"总价",
	"rate":"占比"
	};

	listmodel.assign(lists,fields);
	detailList = listmodel.getPricefree();
	# iSummaryModel.addFormula();
	debug(detailList)
	# debug(iFormulaModel.aSite);

	
					
				
			