# !/usr/bin/env python3
# encoding:UTF-8
# author：杜阳
# ——————————————获得输入输出的键值————————————————————
from  framework.includeList import  *

class parameterModel(object):
	def getParameter(self,file):
		var = __import__("libs.inputVariable")
		inputvar = getattr(var,"inputVariable")
		inputPattern = getParser('inOutmode','inputMode');
		info("输入的模式是"+str(inputPattern))
		outputPattern = getParser('inOutmode','outputMode');
		info("输出的模式是"+str(outputPattern))
		fileSplit = file.rpartition('.')[0].split('_');
		# debug(getattr(inputvar,"exportInput"))
		# debug(getattr(inputvar,"internalInput"));
		# 通过文件名来判断是否是从NHCT导出来的
		if len(fileSplit[-1]) == 8 :
			inputParam = getattr(inputvar,"exportInput");
		else:
			inputParam = getattr(inputvar, (inputPattern+ "Input"));
		
		outputParam = getattr(inputvar, (outputPattern + "Output"));
		# debug(inputParam)
		# debug(outputParam);
		return inputParam , outputParam
		
		
		
		
