# !/usr/bin/env python3
#encode:UTF8
# author：杜阳
# ——————————————————作用：获得归并页——————————————————
from  framework.includeList import  *
import copy;

class mergeModel(object):
	# **************传递数组和字段**************
	def assign(self,lists,fields):
		self.lists = lists;
		self.fields = fields;
		colOrdinal = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
		              'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
		# 先组合成为dict
		self.dCol = dict(zip(fields, colOrdinal));	
		self.getSubtotalIndex();
		# **************获得索引表	# **************
	def getSubtotalIndex (self):
		self.aSite = [];
		self.aSubtotal = [];
		self.aTotal = 0;
		for i , arr in enumerate (self.lists):
			if arr['colorTag'] == 'site':
				self.aSite.append(i);
			elif arr['colorTag'] == 'subtotal':

				self.aSubtotal.append(i);
			elif arr['colorTag'] == 'total':
				self.aTotal = i;
			else:
				continue;

		self.aHeader = 0;
		

	# **************主函数**************
	def getMergeLists (self):
		# 加公式
		self.addFormula();
		# 加总计行
		self.addTotal()
		
		return self.mergeLists;
		# **************将用公式生成的字段转换为数字**************
	def contvert2Value (self):
		# processedArr = self.lists.copy();
		processedArr = copy.deepcopy(self.lists);
		# 转换数量列
		if 'quantity' in processedArr[0].keys():
			for i  , s in enumerate(self.aSite):
				# siteInitial = str(s + 1);
				for j in range(s + 1 , self.aSubtotal[i]-1 + 1):
					processedArr[j]['totalQuantity'] = processedArr[j]['quantity'] * processedArr[s]['quantity'];
		# 转换单价列
		if 'unitsNetListPrice' in processedArr[0].keys() and 'discount' in processedArr[0].keys() :
			for i , s in enumerate(self.aSite):
				for j in range(s+1 , self.aSubtotal[i]-1 + 1):
					if isinstance(processedArr[j]['discount'] , float):
						processedArr[j]['unitsNetPrice'] = processedArr[j]['discount'] * processedArr[j]['unitsNetListPrice'];
					else:
						processedArr[j]['discount'] = 1;
						processedArr[j]['unitsNetPrice'] = processedArr[j]['unitsNetListPrice'];
		return processedArr;
	# # **************删除小计行**************	
	def removeSubtotal(self):
		remainLists = self.contvert2Value().copy();
		for arr in remainLists[::-1]:
			if arr['colorTag'] in ['total','header','subtotal','site']:
				remainLists.remove(arr);
		# debug('remainLists is ' + str (remainLists));
		return remainLists;
	# **************合并相同的BOM或者typeID**************
	def mergeSimilar (self):
		# 删除标题
		remainLists = self.removeSubtotal();
		# 将每一行的'BOM'或者'typeID'作为key，生成一个dict，
		types = [t for t in ['BOM','typeID'] if t in remainLists[0].keys()];
		if types != []:
			field = types[0];
		
		mergeMaps = {};
		removeDuplications = [];
		try:
			# 遍历每一行
			for arr in remainLists:
				val = arr[field];
				# 若是BOM或者typeID没有出现，则新建
				if arr[field] not in mergeMaps.keys():
					mergeMaps[val] = arr;
					removeDuplications.append(val);
				
					if 'unitsNetListPrice' in arr.keys() and arr['unitsNetListPrice'] != 0:
						arr['discount'] = arr['unitsNetPrice'] / arr['unitsNetListPrice'];

				else :
				# 若是出现过，则数量增加
					mergeMaps[val]['totalQuantity'] += arr['totalQuantity'];	
					
		except Exception as data:
			error("原始的Excel格式有问题，可尝试删除有颜色的行再试")
		return mergeMaps;
	
	
	# **************添加公式**************
	def addFormula(self):
		mergeMaps = self.mergeSimilar();
		self.mergeLists = list(mergeMaps.values());
		# 添加首行
		d = self.fields;
		d['colorTag'] = 'header';
		self.mergeLists.insert(0,d);
		
		for i , arr in enumerate(self.mergeLists[1::] , 1):
			arr['unitsNetPrice'] = '=' + self.dCol['unitsNetListPrice'] + str(i + 1 ) + '*' + self.dCol['discount'] + str(i+1);
			arr['totalPrice'] = '=' + self.dCol['unitsNetPrice'] + str(i + 1) + "*" + self.dCol['totalQuantity'] + str(i+1);
			arr['rate'] = '=' + self.dCol['totalPrice'] + str(i+1) + '/' + self.dCol['totalPrice'] + str(len(self.mergeLists)+ 1);
	# **************添加总计行**************
	def addTotal(self):
		# 最后一行
		dict = {};
		for f in self.fields.keys():
			dict [f] = "";
		
		dict['colorTag'] = 'subtotal';
		dict['totalPrice'] = '=SUM('+self.dCol['totalPrice']+'2:'  + self.dCol['totalPrice'] + str(len(self.mergeLists)- 1 + 1) + ")";
		self.mergeLists.append(dict);
		
		
		
		
if __name__ == "__main__":
	iMerge =mergeModel();
	lists = [
	{'ID': 'ID', 'BOM': '产品编码', 'typeID': '产品型号', 'description': '项目名称', 'quantity': '单套数量', 'totalQuantity': '总数量', 'unitsNetListPrice': '目录价', 'discount': '折扣', 'unitsNetPrice': '单价', 'totalPrice': '总价', 'remarks': '备注', 'colorTag': 'header'},
	 {'ID': 1, 'BOM': 'HP DL380 Gen9 24SFF CTO Server [#1]', 'typeID': '', 'description': '', 'quantity': 1.0, 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '=H25', 'unitsNetPrice': '', 'totalPrice': '', 'remarks': '', 'colorTag': 'site'},
	 {'ID': '', 'BOM': '767032-B21', 'typeID': 'ISS', 'description': 'HPE DL380 Gen9 24SFF CTO Server', 'quantity': 1.0, 'totalQuantity': '=$E$2*E3', 'unitsNetListPrice': 15100.0, 'discount': '=H2', 'unitsNetPrice': '=G3*H3', 'totalPrice': '=I3*F3', 'remarks': '', 'colorTag': 'general'},
	 {'ID': '', 'BOM': '767032-B21', 'typeID': 'ISS', 'description': 'HPE DL380 Gen9 24SFF CTO Server', 'quantity': 1.0, 'totalQuantity': '=$E$2*E3', 'unitsNetListPrice': 15100.0, 'discount': '=H2', 'unitsNetPrice': '=G3*H3', 'totalPrice': '=I3*F3', 'remarks': '', 'colorTag': 'general'},
	 {'ID': '', 'BOM': '767032-B21', 'typeID': 'ISS', 'description': 'HPE DL380 Gen9 24SFF CTO Server', 'quantity': 1.0, 'totalQuantity': '=$E$2*E3', 'unitsNetListPrice': 15100.0, 'discount': '=H2', 'unitsNetPrice': '=G3*H3', 'totalPrice': '=I3*F3', 'remarks': '', 'colorTag': 'general'},
	 {'ID': '', 'BOM': '小计', 'typeID': '', 'description': '=C3', 'quantity': '', 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '', 'unitsNetPrice': '', 'totalPrice': '=SUM(J3:J19)', 'remarks': '', 'colorTag': 'subtotal'},
	 {'ID': 2, 'BOM': 'BTO_1', 'typeID': '', 'description': '', 'quantity': 1.0, 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '=H25', 'unitsNetPrice': '', 'totalPrice': '', 'remarks': '', 'colorTag': 'site'},
	 {'ID': '', 'BOM': '455883-B21', 'typeID': 'ISS', 'description': 'HPE BLc 10G SFP+ SR Transceiver', 'quantity': 4.0, 'totalQuantity': '=$E$21*E23', 'unitsNetListPrice': 2900.0, 'discount': '=H21', 'unitsNetPrice': '=G23*H23', 'totalPrice': '=I23*F23', 'remarks': '', 'colorTag': 'general'},
	 {'ID': '', 'BOM': '小计', 'typeID': '', 'description': '=C22', 'quantity': '', 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '', 'unitsNetPrice': '', 'totalPrice': '=SUM(J22:J23)', 'remarks': '', 'colorTag': 'subtotal'},
	 {'ID': '', 'BOM': '总计', 'typeID': '', 'description': '', 'quantity': '', 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': 1, 'unitsNetPrice': '', 'totalPrice': '=SUM(J2:J24)/2', 'remarks': '', 'colorTag': 'total'}];
	dOutput = {
	"ID":"ID",
	"BOM":"产品编码",
	"typeID":"产品型号",
	"description":"项目名称",
	"quantity":"单套数量",
	"totalQuantity":"总数量",
	"unitsNetListPrice":"目录价",
	"discount":"折扣",
	"unitsNetPrice":"单价",
	"totalPrice":"总价",
	"totalListPrice":"总目录价",
	"remarks":"备注",
} ;
	fields = {
		"ID":"序号",
		"description":"产品型号",
		"typeID":"产品编码",
		"quantity":"数量",
		"unitsNetPrice":"单价",
		"totalPrice":"总价",
		"rate":"占比"
};
	iMerge.assign(lists,fields , list(dOutput.keys()),'价格明细清单');
	mergeLists = iMerge.getMergeLists();
	# iSummaryModel.addFormula();
	debug(mergeLists)
	# debug(iFormulaModel.aSite);

	
	
	
	
	
	
	