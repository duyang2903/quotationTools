# !/usr/bin/env python3
#encode:UTF-8
# author：杜阳
#######################作用：添加公式#######################
from  framework.includeList import *
class formulaModel(object):
	# **************赋值：将数组传递进去**************
	def assign (self, lists, outputKeys):
		self.lists = lists;
		self.outputKeys = outputKeys;
		colOrdinal = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
		              'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
		# 先组合成为dict
		self.dCol = dict(zip(self.outputKeys, colOrdinal));
	# **************总函数**************
	def addFormula (self):
		# 获得文档结构表
		self.getSubtotalIndex();
		try:
			# 对site进行修改
			self.rehandleSite();
		except Exception as data:
			error("输入Excel格式和对应的模式不匹配"+str(data));
		# 对数量列进行修改
		self.rehandleQty();
		# 加上总数量列
		self.addTotalQty();
		# 修改折扣列
		self.rehandleDisc();
		# 若configure.conf中设置保存原有折扣
		if getParser("inOutmode","reserveDisc") == "True":
			self.reserveDisc();
		# 修改单价列
		self.rehandleUnitPrice();
		# 加上总价列公式
		self.addTotalPrice();
		# 如果是平安投标mode
		if getParser('inOutmode','outputMode') == "pingan":
			self.addTaxExclusivePrice();
		# 加上小计行的公式
		self.addSubtotal();
		# 加总计行公式
		self.addTotal();
		# 重构ID列
		self.rehandleID();
		# 加一些常用的备注
		self.addRemark();
		# 替换光模块
		self.replaceSFP();
		return self.lists;
		
	# **************获得文档结构表**************
	def getSubtotalIndex (self):
		self.aSite = [];
		self.aSubtotal = [];
		self.aTotal = 0;
		# 遍历数组，根据colorTag来进行判断
		for i , arr in enumerate (self.lists):
			if arr['colorTag'] == 'site':
				self.aSite.append(i);
			elif arr['colorTag'] == 'subtotal':

				self.aSubtotal.append(i);
			elif arr['colorTag'] == 'total':
				self.aTotal = i;
			else:
				continue;

		self.aHeader = 0;
		
		debug("aSite = " + str(self.aSite));
		debug("aSubtotal= " +  str(self.aSubtotal));
		debug('aTotal=' + str(self.aTotal)) ;
		
	# **************重构Site行**************
	def rehandleSite (self):
		# 从aSite数组里面取出site所在行的行号
		for i , s in enumerate(self.aSite):
			# 若site中含有+，则以+进行拆分
			if 'BOM'in self.lists[0].keys() and self.lists[s]['BOM'].find('+') != -1:
				sites = self.lists[s]['BOM'].split('+');
				self.lists[s]['BOM'] = sites[-1];				
			# 若BOM列不需要输出，则把BOM列复制到typeID列中
			if 'BOM' not in self.outputKeys and  'BOM'  in self.lists[0].keys():
				self.lists[s]['typeID'] = self.lists[s]['BOM'];
				
			
			# if 'totalQuantity' in self.outputKeys  and getParser("outputmode","pattern") in ['internal','HPE','client']:
				# self.lists[s]['totalQuantity'] = "（套）";
						
	# **************重构数量列**************
	def rehandleQty (self):
		try :
			# 先对表格进行遍历，如果出现数量处为空的情况，填1进去
			for arr in self.lists:
				if arr['quantity'] == "" and arr['colorTag'] == "general":
					arr['quantity'] = 1;
			
			diff = [i for i in ['BOM','typeID'] if i in self.outputKeys];
			tag = diff[0];
			# 从aSite数组里面取出site所在行的行号
			for i , s in enumerate(self.aSite):
				# 如果site标题所在行的quantity为空，同时在'BOM'那一列没有BTO的字样时
				if self.lists[s]['quantity'] == "" and self.lists[s][tag].find("BTO") == -1:
					# 获得到了套数
					Qty = int (self.lists[s+1]['quantity']);
					# 配置开始行均为主机，所以他的quantity实际就是套数
					self.lists[s]['quantity'] = Qty
					# 将剩下的都除以套数
					for j in range(s + 1 , self.aSubtotal[i]):
						self.lists[j]['quantity']= int(self.lists[j]['quantity'])/Qty;
				# 如果BOM列含有BTO的字样，说明以下的配置是BTO的，所以置1
				elif self.lists[s][tag].find("BTO") != -1 and self.lists[s]['quantity']\
						== "":
					self.lists[s]['quantity'] = 1;
				
		except Exception as data:
			error(data);
	
	# # **************添加总数量列**************
	def addTotalQty (self):
		# 若输出的键值中有quantity
		if 'quantity' in self.outputKeys :
			for i , s in enumerate(self.aSite):
				# siteInitial代表表格中显示的site起始行（表格是从1开始）
				siteInitial = str(s + 1); 
				for j in range(s + 1 , self.aSubtotal[i]-1 + 1):
					self.lists[j]['totalQuantity'] = '=$' + self.dCol['quantity'] + "$" + siteInitial + "*" + self.dCol['quantity'] + str (j + 1);
		# # 若输出的没有quantity这个键值，则totalQuantity列用数值代替
		elif 'quantity' in self.lists[0].keys():
			for i , s in enumerate(self.aSite):
				for j in range(s + 1 , self.aSubtotal[i]-1 + 1):
					self.lists[j]['totalQuantity'] = self.lists[s]['quantity'] * self.lists[j]['quantity'];
			
	# **************重构折扣列#######################
	def rehandleDisc(self):
		# 若输出含有折扣
		if 'discount' in self.outputKeys:
			# 在总计行上填上100%
			self.lists[-1]['discount'] = 1;
			for i , s in enumerate(self.aSite):
			# 所有的site上的off与总计行的off相等
				self.lists[s]['discount'] = '=' + self.dCol['discount'] + str(self.aTotal + 1);
			# 详细配置的disc列与site行的相等
				siteInitial = str(s + 1 );
				for j in range(s + 1 , self.aSubtotal[i] - 1+ 1 ):
					self.lists[j]['discount'] = '=' + self.dCol['discount'] + siteInitial;
					
	# **************保留修改后的折扣**************
	
	def reserveDisc(self):
		# 若是单目录价列不存在
		if 'unitsNetListPrice' not in self.outputKeys or 'unitsNetListPrice' not in self.lists[0].keys():
			return;
			
		try:
			for arr in self.lists:
				# 若是目录单价和单价列任意一个为空，则置1
				if arr['colorTag'] == "general" and arr['unitsNetListPrice'] == "" or arr['unitsNetPrice'] == "":
					arr["discount"] = 1;
				# 其他情况中，若单价和目录单价不存在为0的情况
				if arr['colorTag'] == 'general' and arr['unitsNetListPrice'] != arr['unitsNetPrice'] and arr['unitsNetListPrice'] != 0 and arr['unitsNetPrice'] != 0:
					arr['discount']  = arr['unitsNetPrice'] / arr['unitsNetListPrice'];
		except Exception as data:		
			error("存在为空的价格"+str(data));
				
	# # **************重构单价列**************
	def rehandleUnitPrice(self):
		try :
			for i , s in enumerate(self.aSite):
				siteInitial = str(s + 1 );
				for j in range(s + 1 , self.aSubtotal[i] - 1+ 1 ):
					self.lists[j]['unitsNetPrice'] = '=' + self.dCol['unitsNetListPrice'] + str(j+1) + "*" + self.dCol['discount'] + str(j + 1 );				
				
		except Exception as data:
			error('缺少price字段' + str(data));
	
	# **************添加总计行**************
	def addTotalPrice (self):
		try :
			for i , s in enumerate(self.aSite):
				siteInitial = str(s + 1 );
				for j in range(s + 1 , self.aSubtotal[i] - 1 + 1):
					self.lists[j]['totalPrice'] = '=' + self.dCol['unitsNetPrice'] + str(j+1) + "*" + self.dCol['totalQuantity'] + str(j+1);
				
		except Exception as data:
			error('缺少price字段' + str(data));
	# 	# **************添加不含税列**************
	def addTaxExclusivePrice(self):
		try :
			for i , s in enumerate(self.aSite):
				siteInitial = str(s + 1 );
				for j in range(s + 1 , self.aSubtotal[i] - 1 + 1):
					self.lists[j]['taxExclusivePrice'] = '=' + self.dCol['unitsNetPrice'] + str(j+1) + "/1.17";
					
					self.lists[j]['totalTaxExcPrice'] = '=' + self.dCol['taxExclusivePrice'] + str(j+1) + "*" + self.dCol['totalQuantity'] + str(j+1);
					
					self.lists[j]['total'] = '=' + self.dCol['totalPrice'] + str(j+1)
				
		except Exception as data:
			error('缺少price字段' + str(data));		
		
	# **************加上小计行的公式**************
	def addSubtotal (self):
		try:
			# 看typeID或者description谁在输入的列中
			aDiff = [i for i in ['typeID', 'description'] if i in self.outputKeys];
			tag = aDiff[0];
			# 在小计行的typeID或者description位处加上配置主机的型号
			for i,sub  in enumerate(self.aSubtotal):
				siteInitial = self.aSite[i] + 1;
				siteEnd = sub - 1;
				self.lists[sub][tag] = '';
				if 'typeID' in self.outputKeys and 'BOM' not in self.outputKeys:
					self.lists[sub]['typeID'] = '小计';
					
				self.lists[sub]['totalPrice'] = '=SUM(' + self.dCol['totalPrice'] + str(siteInitial + 1) + ":" + self.dCol['totalPrice'] + str(siteEnd + 1) + ")";
				# 单套总价格
				if getParser('inOutmode','outputMode') in ["internal",'HPE']:
					self.lists[sub]['unitsNetPrice'] = '=SUMPRODUCT(' + self.dCol['unitsNetPrice'] + str(siteInitial + 1) + ":" + self.dCol['unitsNetPrice'] + str(siteEnd + 1) + "," + self.dCol['quantity'] + str(siteInitial + 1 ) + ":" + self.dCol['quantity'] + str(siteEnd + 1) + ")";
		except Exception as data:
			error("缺少typeID,description" + str(data));
		
	# **************获得总价行**************
	def addTotal (self)	:
		if 'typeID' in self.outputKeys and 'BOM' not in self.outputKeys:
			self.lists[-1]['typeID'] = '总计';
			
		self.lists[-1]['totalPrice'] = '=SUM(' + self.dCol['totalPrice'] + '2:' + self.dCol['totalPrice'] + str(self.aTotal) + ')/2';
	
	# **************重构ID列**************
	def rehandleID(self):
		if 'ID' not  in self.lists[0].keys():
			return;
		
		if getParser('inOutmode','bid') == 'False':
			info("普通模式");
			for i , s in enumerate(self.aSite):
				self.lists[s]['ID'] = i + 1; 
		# 若是投标模式
		else:		
			info('投标模式，请注意不要再通过此文件生成其他文件');
			for i , s in enumerate (self.aSite):
				self.lists[s]['ID'] = i+1;
				count = 0.1;
				for j in range(s + 1 , self.aSubtotal[i]- 1 + 1) :
					self.lists[j]['ID'] = i + 1 + count;
					count += 0.1;
	# **************替换SFP**************			
	def replaceSFP(self):
		if getParser('ajustableParam','replaceSFP') != 'True':
			return;
			
		if 'typeID' not in self.lists[0].keys():
			error('replaceSFP没有执行，因为没有typeID字段')
			return;
			
		var = __import__("libs.inputVariable")
		inputvar = getattr(var,"inputVariable")
		mReplace = getattr(inputvar,"mReplace");
		for arr in self.lists:
			if arr['colorTag'] == 'general' and arr['typeID'] in mReplace.keys():
				info("替换了光模块")
				arr['typeID'] = mReplace[arr['typeID']];
	
	# **************添加备注**************
	def  addRemark(self):
		# if getParser('inOutmode','addRemark') != 'True':
			# return
		if getParser('inOutmode','addRemark') == "DB":
			return
		elif getParser('inOutmode','addRemark') == 'inputvar':			
			var = __import__("libs.inputVariable")
			inputvar = getattr(var,"inputVariable")
			mRemarks = getattr(inputvar,"mRemarks");
			for arr in self.lists:
			# 如果在inputvariable文件里面能找到对应的备注描述
				if arr['colorTag'] == 'general' and 'typeID'in self.lists[0].keys() and arr[
						'typeID'] in mRemarks.keys():
					arr['remarks'] = mRemarks[arr['typeID']];
				elif  arr['colorTag'] == 'general' and 'typeID'in self.lists[0].keys():
					arr['remarks'] = "";
		else:
			for arr in self.lists:
				if arr['colorTag'] == 'general' and 'typeID'in self.lists[0].keys():
					arr['remarks'] = "";			
		
		
if __name__ == "__main__":
	iFormulaModel = formulaModel ();
	lists = [
	{'ID': '序号', 'BOM': '产品编码', 'typeID': '产品型号', 'description': '项目名称', 'quantity': '数量', 'unitsNetListPrice': '标准价(RMB)', 'discount': '折扣', 'unitsNetPrice': '单价(RMB)', 'totalPrice': '总价(RMB)', 'totalListPrice': '标准价总价(RMB)', 'PL': '产线', 'waston': 'WATSON_LINE_ITEM_ID', 'remarks': '备注', 'totalQuantity': '', 'colorTag': 'header'},
	{'ID': '1', 'BOM': 'H3CI-30585-00+HP DL380 Gen9 24SFF CTO Server [#1]', 'typeID': '', 'description': '', 'quantity': '', 'unitsNetListPrice': '', 'discount': '', 'unitsNetPrice': '', 'totalPrice': '', 'totalListPrice': '', 'PL': '', 'waston': '', 'remarks': '', 'totalQuantity': '', 'colorTag': 'site'},
	{'ID': '', 'BOM': '767032-B21', 'typeID': 'ISS', 'description': 'HPE DL380 Gen9 24SFF CTO Server', 'quantity':2.0, 'unitsNetListPrice': 15100.0, 'discount': 1.0, 'unitsNetPrice': 1234.0, 'totalPrice': 15100.0, 'totalListPrice': 15100.0, 'PL': 'SY', 'waston': '2', 'remarks': '', 'totalQuantity': '', 'colorTag': 'general'},
	{'ID': '', 'BOM': '767032-B21', 'typeID': 'ISS', 'description': 'HPE DL380 Gen9 24SFF CTO Server', 'quantity':14, 'unitsNetListPrice': 15100.0, 'discount': 1.0, 'unitsNetPrice': 1510.0, 'totalPrice': 15100.0, 'totalListPrice': 15100.0, 'PL': 'SY', 'waston': '2', 'remarks': '', 'totalQuantity': '', 'colorTag': 'general'},
	{'ID': '', 'BOM': '小计', 'typeID': 'H3CI-30585-00+BTO_1', 'description': '', 'quantity': '', 'unitsNetListPrice': '', 'discount': '', 'unitsNetPrice': '', 'totalPrice': 13810.0, 'totalListPrice': 13810.0, 'PL': '', 'waston': '', 'remarks': '', 'totalQuantity': '', 'colorTag': 'subtotal'},
	{'ID': '1', 'BOM': 'BTO DL380 Gen9 24SFF CTO Server [#1]', 'typeID': '', 'description': '', 'quantity': '', 'unitsNetListPrice': '', 'discount': '', 'unitsNetPrice': '', 'totalPrice': '', 'totalListPrice': '', 'PL': '', 'waston': '', 'remarks': '', 'totalQuantity': '', 'colorTag': 'site'},
	{'ID': '', 'BOM': '767032-B21', 'typeID': 'ISS', 'description': 'HPE DL380 Gen9 24SFF CTO Server', 'quantity': 20, 'unitsNetListPrice': 15100.0, 'discount': 1.0, 'unitsNetPrice': 15100.0, 'totalPrice': 15100.0, 'totalListPrice': 15100.0, 'PL': 'SY', 'waston': '2', 'remarks': '', 'totalQuantity': '', 'colorTag': 'general'},
	{'ID': '', 'BOM': '小计', 'typeID': 'H3CI-30585-00+BTO_1', 'description': '', 'quantity': '', 'unitsNetListPrice': '', 'discount': '', 'unitsNetPrice': '', 'totalPrice': 13810.0, 'totalListPrice': 13810.0, 'PL': '', 'waston': '', 'remarks': '', 'totalQuantity': '', 'colorTag': 'subtotal'}, 
	{'ID': '', 'BOM': '总计', 'typeID': '', 'description': '', 'quantity': '', 'unitsNetListPrice': '', 'discount': '', 'unitsNetPrice': '', 'totalPrice': 158660.0, 'totalListPrice': 158660.0, 'PL': '', 'waston': '', 'remarks': '', 'totalQuantity': '', 'colorTag': 'total'}
	
	];	
	dOutput = {
	"ID":"ID",
	"BOM":"产品编码",
	"typeID":"产品型号",
	"description":"项目名称",
	"quantity":"单套数量",
	"totalQuantity":"总数量",
	"unitsNetListPrice":"目录价",
	"discount":"折扣",
	"unitsNetPrice":"单价",
	"totalPrice":"总价",
	"totalListPrice":"总目录价",
	"remarks":"备注",
} ;
	iFormulaModel.assign(lists,list(dOutput.keys()));
	iFormulaModel.addFormula();
	debug(iFormulaModel.lists)
	# debug(iFormulaModel.aSite);

	
	
	
	
	
	
	
	