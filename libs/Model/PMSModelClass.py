# !/usr/bin/env python3
#encode:UTF8
# author：杜阳
# ————————————————作用：生成PMS sheet————————————————————————
from  framework.includeList import  *
import copy;
class PMSModel(object):
	def assign(self,lists,fields):
		self.lists = copy.deepcopy(lists);
		self.fields = fields;
		colOrdinal = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
		              'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
		# 先组合成为dict
		self.dCol = dict(zip(fields, colOrdinal));
	# **************主函数**************
	def getPMS(self):
		# 加列
		self.addColumns();
		# 删除小计行
		self.removeSubtotal();
		# 重构site
		self.rehandleSite();
		# 重构标题行
		self.rehandleheader();
		return self.lists;
		
	# **************加列**************
	def addColumns (self):
		diffList = [d  for d in self.fields if d not in self.lists[0].keys()];
		diff = {
			"discount1":1,
			"discount2":1,
			"discount3":1,
			"discount4":1		
		}
		
		for arr in self.lists:
		# general类型和非general类型区别对待
		# general类型需要在diff中查找
			if arr['colorTag'] == "general":
				for d in diffList:
					# 查找到相应的字段则直接复制，没查找到的则为空
					arr[d] = diff.get(d) if diff.get(d) != None  else "";
			else:
				for d in diffList:
					arr[d] = "";
	
	# **************将小计行、总计行删除**************
	def removeSubtotal(self):
		# 要删除须从后往前遍历
		for arr in self.lists[::-1]:
			if arr['colorTag'] == "subtotal" or arr['colorTag'] == "total":
				self.lists.remove(arr);
				
	# **************将Site行进行修改	**************
	def rehandleSite(self):
		try:
			for arr in self.lists:
				if arr['colorTag'] == "site":
					arr['siteNames'] = arr['BOM'] if arr['BOM'] != "" else arr['typeID'];
					arr['BOM'] = ""
					arr['typeID'] = ""
					arr['discount4'] = "套数";

		except Exception as data:
			error("PMS sheet：之前添加列出现了问题" + str(data))
	# **************重构标题行**************
	def rehandleheader(self):
		for k,v in self.fields.items():
			self.lists[0][k] = v;
		
	
			
if __name__ == "__main__":
	pmsmodel =PMSModel();
	lists = [
	{'ID': 'ID', 'BOM': '产品编码', 'typeID': '产品型号', 'description': '项目名称', 'quantity': '单套数量', 'totalQuantity': '总数量', 'unitsNetListPrice': '目录价', 'discount': '折扣', 'unitsNetPrice': '单价', 'totalPrice': '总价', 'remarks': '备注', 'colorTag': 'header'},
	 {'ID': 1, 'BOM': 'HP DL380 Gen9 24SFF CTO Server [#1]', 'typeID': '', 'description': '', 'quantity': 1.0, 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '=H25', 'unitsNetPrice': '', 'totalPrice': '', 'remarks': '', 'colorTag': 'site'},
	 {'ID': '', 'BOM': '767032-B21', 'typeID': 'ISS', 'description': 'HPE DL380 Gen9 24SFF CTO Server', 'quantity': 1.0, 'totalQuantity': '=$E$2*E3', 'unitsNetListPrice': 15100.0, 'discount': '=H2', 'unitsNetPrice': '=G3*H3', 'totalPrice': '=I3*F3', 'remarks': '', 'colorTag': 'general'},
	 {'ID': '', 'BOM': '小计', 'typeID': '', 'description': '=C3', 'quantity': '', 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '', 'unitsNetPrice': '', 'totalPrice': '=SUM(J3:J19)', 'remarks': '', 'colorTag': 'subtotal'},
	 {'ID': 2, 'BOM': 'BTO_1', 'typeID': '', 'description': '', 'quantity': 1.0, 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '=H25', 'unitsNetPrice': '', 'totalPrice': '', 'remarks': '', 'colorTag': 'site'},
	 {'ID': '', 'BOM': '455883-B21', 'typeID': 'ISS', 'description': 'HPE BLc 10G SFP+ SR Transceiver', 'quantity': 4.0, 'totalQuantity': '=$E$21*E23', 'unitsNetListPrice': 2900.0, 'discount': '=H21', 'unitsNetPrice': '=G23*H23', 'totalPrice': '=I23*F23', 'remarks': '', 'colorTag': 'general'},
	 {'ID': '', 'BOM': '小计', 'typeID': '', 'description': '=C22', 'quantity': '', 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '', 'unitsNetPrice': '', 'totalPrice': '=SUM(J22:J23)', 'remarks': '', 'colorTag': 'subtotal'},
	 {'ID': '', 'BOM': '总计', 'typeID': '', 'description': '', 'quantity': '', 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': 1, 'unitsNetPrice': '', 'totalPrice': '=SUM(J2:J24)/2', 'remarks': '', 'colorTag': 'total'}];
	dOutput = {
	"ID":"ID",
	"BOM":"产品编码",
	"typeID":"产品型号",
	"description":"项目名称",
	"quantity":"单套数量",
	"totalQuantity":"总数量",
	"unitsNetListPrice":"目录价",
	"discount":"折扣",
	"unitsNetPrice":"单价",
	"totalPrice":"总价",
	"totalListPrice":"总目录价",
	"remarks":"备注",
} ;
	fields = {
	"siteNames":"组合名称",
	"encode":"编码",
	"name":"名称",
	"BOM":"BOM编码",
	"typeID":"名称",
	"description":"描述",
	"form":"类型",
	"unitsNetListPrice":"单价",
	"discount1":"折扣",
	"discount2":"折扣",
	"discount3":"折扣",
	"discount4":"折扣",
	"quantity":"数量"
};
	pmsmodel.assign(lists,fields );
	pmsLists = pmsmodel.getPMS();
	# iSummaryModel.addFormula();
	debug(pmsLists)
	# debug(pmsLists.aSite);

	
	
	
	
	
	
		
			