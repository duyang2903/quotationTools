# !/usr/bin/env python3
#encode:UTF-8
# author：杜阳
# **************作用：在加公式之前，对读入的数组进行处理**************
# 删除部分行
# 加上小计行、总计行
from  framework.includeList import *

class rehandleModel(object):
		
	def assign (self , lists):
		self.lists = lists;
	# **************主函数**************
	def doRehandel (self,diffList):
		# 删除含有#的行
		self.removeRows();
		# 加行
		self.addRow();
		# 加colorTag
		self.addColorTag();
		# 加列
		self.addColumns(diffList);
		return self.lists;
		
	# **************删除行**************
	def removeRows(self):
		# 逆序遍历，否者一边删除一边iterator就改变了
		for aList in self.lists[::-1]:
			try:
				if set(['BOM','typeID','ID']) < set(aList.keys()) and str (aList['BOM']).find("#") != -1 and aList['ID'] == "" and aList['typeID'] == "":
					self.lists.remove(aList);
					info("删除了含有#的行");
				elif 'description' in aList.keys() and str(aList['description']).find('Factory integrated') != -1:
					self.lists.remove(aList);
					info("删除了含有Factory integrated行");
					# 单独删除NHCT导出模板中的含截止日期行
				elif 'unitsNetPrice' in aList.keys() and str(aList['unitsNetPrice']).find(u'截止日期') != -1:
					self.lists.remove(aList);
					info("删除了含有截止日期的行");
				elif 'ID' in aList.keys() and str(aList['ID']).find(u'价格明细清单')  != -1:
					self.lists.remove(aList);
					info("删除了含有价格明细清单的行");
					# 删除空行，取出所有的values，通过map全部变为str类型，然后转换为list，最后串接在一起。
				elif len("".join(list(map(str,aList.values())))) == 0 :
					self.lists.remove(aList);
					info("删除空行");
				else:
					continue;
			except Exception as data:
				error("删除空行时，超出表格范围"+str(data));
			
	# **************子函数：获得一空行**************			
	def getRow (self , key , value):
		# 先全部填上空白
		row = {};
		for k in self.lists[0].keys():
			row[k] = "";
		
		row[key] = value;
		return row;
	# **************加上小计行、总计行**************
	def addRow(self):
		# 遍历lists，插入小计行、总计行
		try:
			aDiff = [i for i in ['BOM','typeID','description']  if i in self.lists[0].keys()];
			colTag = aDiff[0];
			for i in range(len(self.lists) - 1 , 1 , -1):
				list = self.lists[i];
				if list['ID']  != "" and self.lists[i-1][colTag] != '小计':
					self.lists.insert(i,self.getRow(colTag,'小计'));
					info ('在第'+str(i)+'行增加了小计行')

			if self.lists[-1][colTag] != '总计':
				self.lists.append(self.getRow(colTag,'总计'));
				info ('在最后一行增加了总计行')

			if self.lists[-2][colTag] != '小计':
				self.lists.insert(len(self.lists)-1 , self.getRow(colTag,'小计'));
				info ('在倒数第二行增加了小计行')
		except Exception as data:
			error(data);
			error ("addRow函数中")


	# **************加列	**************		
	def addColumns (self , diffList):
		var = __import__("libs.inputVariable");
		inputvar = getattr(var , "inputVariable");
		diff = getattr(inputvar , "diff");
		for arr in self.lists:
			if arr['colorTag'] == "general":
				for d in diffList:
					# 查找到相应的字段则直接复制，没查找到的则为空
					arr[d] = diff.get(d) if diff.get(d) != None  else "";
			else:
				for d in diffList:
					arr[d] = "";
					
	# **************加颜色标签**************		
	def addColorTag (self)		:
		try:
			aDiff = [i for i in ['BOM','typeID','description']  if i in self.lists[0].keys()];
			colTag = aDiff[0];
			for aList in self.lists:
				if aList[colTag] == "小计":
					aList['colorTag'] = "subtotal";
				elif aList[colTag] == "总计":
					aList['colorTag'] = "total";
				elif aList['ID'] != "":
					aList['colorTag'] = 'site';
				else:
					aList['colorTag'] = "general";
					
			self.lists[0]['colorTag'] = "header"
		except Exception as data:
			error(data)
			error("缺少字段");


	
if __name__ == "__main__":
	lists = [
	{'ID': '', 'BOM': '', 'typeID': '', 'description': '', 'num': '', 'listprice': '', 'off': '', 'price': '20170707-招银云创服务器零散采购7 截止日期：2017/08/17', 'totalPrice': '', 'totalListPrice': '', 'productLine': '', 'waston': '', 'addOn': ''},
	{'ID': '', 'BOM': '', 'typeID': '', 'description': '', 'num': '', 'listprice': '', 'off': '', 'price': '', 'totalPrice': '', 'totalListPrice': '', 'productLine': '', 'waston': '', 'addOn': ''}, 
	{'ID': '', 'BOM': '', 'typeID': '', 'description': '', 'num': '', 'totalNum': '', 'listprice': '', 'off': '', 'price': '', 'totalPrice': '', 'totalListPrice': '', 'addOn': ''},
	{'ID': '', 'BOM': '单套数量', 'typeID': '产品编码', 'description': '产品型号', 'num': '项目名称', 'totalNum': '总数量', 'listprice': '目录价', 'off': '折扣', 'price': '单价', 'totalPrice': '总价', 'totalListPrice': '总目录价', 'addOn': '产线'}, 
	{'ID': 1.0, 'BOM': 2.0, 'typeID': 'H3CI-31518-00+BTO_1', 'description': '', 'num': '', 'totalNum': '', 'listprice': '', 'off': 0.0, 'price': '', 'totalPrice': '', 'totalListPrice': '', 'addOn': ''},
	{'ID':'' , 'BOM':'#', 'typeID': '', 'description': '', 'num': '', 'totalNum': '', 'listprice': '', 'off': 0.0, 'price': '', 'totalPrice': '', 'totalListPrice': '', 'addOn': ''},
	{'ID':'' , 'BOM':'', 'typeID': 'H3CI-31518-00+BTO_1', 'description': 'Factory integrated', 'num': '', 'totalNum': '', 'listprice': '', 'off': 0.0, 'price': '', 'totalPrice': '', 'totalListPrice': '', 'addOn': ''},
	{'ID': '', 'BOM': '', 'typeID': '', 'description': '', 'num': '', 'listprice': '', 'off': '', 'price': '20170707-招银云创服务器零散采购7 截止日期：2017/08/17', 'totalPrice': '', 'totalListPrice': '', 'productLine': '', 'waston': '', 'addOn': ''}
	];
	rM = rehandleModel();
	rM.assign(lists);
	rM.doRehandel(['du']);
	# rM.addCol('du','yang');
	# rM.delRow();
	
	# # rM.getRow("BOM","小计")
	# rM.addRow();
	# rM.removeRows();
	# rM.addColumns(['du']);
	# rM.addColumns(['d']);
	# rM.
	debug(rM.lists)
	# print(rM);
	
