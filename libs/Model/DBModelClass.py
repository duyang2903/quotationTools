# !/usr/bin/env python3
#encode:UTF-8
# author：杜阳

# 作用：从数据库里面读数据
from  framework.includeList import *

class  DBModel(object):
	# *************初始化：连接数据库&读取数据库configure文件*************
	def __init__(self):
		var = __import__("libs.inputVariable")
		inputvar = getattr(var,"inputVariable")
		dbConfig = getattr(inputvar,"dbConfig");
		self.DB = MySQL();
		self.DB.connect(dbConfig);
		
		
	# *************将list传递进来*************
	def assign (self, lists, outputKeys):
		self.lists = lists;
		self.outputKeys = outputKeys;
		colOrdinal = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M','N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
		# 先组合成为dict
		self.dCol = dict(zip(self.outputKeys, colOrdinal));
	# # *************获得数据库的结构*************
	# 返回：数据库含有的key值
	def getStructure(self):
		# self.table = getParser('tname','table');
		self.table = getParser('db','table');
		sql = "select COLUMN_NAME from information_schema.COLUMNS where table_name ='" + self.table + "'";
		debug(sql);
		# [('ID',), ('BOM',), ('typeID',), ('discription',), ('listprice',)]
		rs = self.DB.findOne(sql);
		# 把输出的转换为数组
		self.fields = [];
		try:
			for s in rs:
				self.fields.append(s[0]);
		except Exception as data:
			error("表结构不存在");
		
		if 'ID' in self.fields:
			self.fields.remove('ID');
		
		
	#************* 遍历每一行，组装SQL，进行查找*************
	def findAll(self):			
		if self.DB.conn == False:
			return;
		# queryfield指的是查找的字段:typeID或者BOM
		field = getParser('db','queryfield');
		headerSQL = "`" + "`,`".join(self.fields) + "`";
		initialSQL = "select " +headerSQL+ " from " + self.table + " where " ;
		# 编历数组
		for arr in self.lists:
			# 只有在general行才查找
			if arr['colorTag'] == 'general':
				# 若此时的typeID对应的值为空，则选用BOM作为查询的字段
				if field == "typeID" and str(arr[field]) == "":
					querySQL = initialSQL + "`BOM` = " + "'"+ str(arr["BOM"]).lstrip().rstrip() + "'";
				# 若此时的BOM对应的值为空，则选用typeID作为查询的字段
				elif field == "BOM" and str(arr[field]) == "":
					querySQL = initialSQL  + + "`typeID` = " + "'"+ str(arr["typeID"]).lstrip().rstrip() + "'";
				else:
				# 其他时候直接使用读取的字段
					querySQL = initialSQL + "`" +field + "`="+ "'"+ str(arr[field]).lstrip().rstrip() + "'";
				debug(querySQL);
				rs = self.DB.findOne(querySQL);
				debug(rs);
				# 将查找以后的结果添加到原数组中
				# 若没有查找到则添加备注
				if rs == []:
					arr['remarks'] = "没有查找到"
				else:
					# arr['remarks']="从数据库"
					# 首先将查询到的值与原数据库的字段组成键值对
					rsDict = dict(zip(self.fields , list(rs[0])));
					for k in self.fields:
						# 若为单目录价或者单价字段
						if k == "unitsNetListPrice" or k == "unitsNetPrice":
							try:
								arr[k] = float(rsDict[k]);
							except Exception:
								error('数据库中的目录价不为float类型');
						else:
							# 其他字段保持原样
							arr[k] = rsDict[k];
							arr['remarks'] = "从数据库";
			
	# **************插入函数**************
	def insert (self):
		if self.DB.conn == False:
			return;		
		# 首先需要查询是否存在此行记录
		# queryfield指的是查找的字段:typeID或者BOM
		field = getParser('db','queryfield');
		headerSQL = "`" + "`,`".join(self.fields) + "`";
		initialSQL = "select " +headerSQL+ " from " + self.table + " where " ;
		# 编历数组
		for arr in self.lists:
			# 只有在general行才查找
			if arr['colorTag'] == 'general':
				# 若此时的typeID对应的值为空，则选用BOM作为查询的字段
				if field == "typeID" and str(arr[field]) == "":
					querySQL = initialSQL + "`BOM` = " + "'"+ str(arr["BOM"]).lstrip().rstrip() + "'";
				# 若此时的BOM对应的值为空，则选用typeID作为查询的字段
				elif field == "BOM" and str(arr[field]) == "":
					querySQL = initialSQL  +  "`"+ "`typeID` = " + "'"+ str(arr["typeID"]).lstrip().rstrip() + "'";
				else:
				# 其他时候直接使用读取的字段
					querySQL = initialSQL + "`" +field + "`="+ "'"+ str(arr[field]).lstrip().rstrip() + "'";		
					
				# 获得查询结果	
				rs = self.DB.findOne(querySQL);
				if rs == []:
					# 把需要插入的组成一个键值对
					insertArr = {};
					# self.fields指的是数据库的字段
					for f  in self.fields :
						insertArr [f] = arr[f];
						
					self.DB.insert(self.table,insertArr);
					arr["remarks"] = "新增";
				else:
					# 若查询到，则需要比较是否目录价发生了改变。
					# 若改变了，则更新，同时加上备注，
					rsDict = dict(zip(self.fields , list(rs[0])));
					arr["remarks"] = "";
					try:
						arr['remarks'] = "已存在";
						if arr['unitsNetListPrice'] != rsDict['unitsNetListPrice']:
							updateArr = {};
							for f in self.fields:
								updateArr[f] = arr[f];
								
							where = " where " + "`BOM` = " + "'"+ str(arr["BOM"]).lstrip().rstrip() + "'";
							self.DB.updateTable(self.table,updateArr,where);
							arr['remarks'] = arr["unitsNetListPrice"] - rsDict["unitsNetListPrice"];
		
					except Exception as data:
						error("查询或新增出现问题");
					# finally:
						# # 不管如何都需要关闭DB
						# self.DB.close();
	
	def insertLists(self):
		try:
			self.getStructure();
			self.insert();
		except Exception as data:
			error(data);
		finally:
			self.DB.close();
			
			
						
	# 总体调用函数
	def replaceLists(self):
		try:
			# 获得数据库的结构
			self.getStructure();
			# 从数据库中拉取数据
			self.findAll();
		except Exception as data:
			error(data)
		finally:
			# 不管如何都需要关闭DB
			self.DB.close();
	

	
	

if __name__ == "__main__":
	dbModel = DBModel();
	lists = [
	{'ID': 'ID', 'BOM': '产品编码', 'typeID': '产品型号', 'description': '项目名称', 'quantity': '单套数量', 'totalQuantity': '总数量', 'unitsNetListPrice': '目录价', 'discount': '折扣', 'unitsNetPrice': '单价', 'totalPrice': '总价', 'remarks': '备注', 'colorTag': 'header'},
	 {'ID': 1, 'BOM': 'HP DL380 Gen9 24SFF CTO Server [#1]', 'typeID': '', 'description': '', 'quantity': 1.0, 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '=H25', 'unitsNetPrice': '', 'totalPrice': '', 'remarks': '', 'colorTag': 'site'},
	 {'ID': '', 'BOM': 'RT-MSR810-10-PoE', 'typeID': 'RT-MSR810-10-PoE', 'description': 'HPE DL380 Gen9 24SFF CTO Server', 'quantity': 1.0, 'totalQuantity': '=$E$2*E3', 'unitsNetListPrice': 15100.0, 'discount': '=H2', 'unitsNetPrice': '=G3*H3', 'totalPrice': '=I3*F3', 'remarks': '', 'colorTag': 'general'},
	 {'ID': '', 'BOM': '767032-B21', 'typeID': 'ISS', 'description': 'HPE DL380 Gen9 24SFF CTO Server', 'quantity': 1.0, 'totalQuantity': '=$E$2*E3', 'unitsNetListPrice': 15100.0, 'discount': '=H2', 'unitsNetPrice': '=G3*H3', 'totalPrice': '=I3*F3', 'remarks': '', 'colorTag': 'general'},
	 {'ID': '', 'BOM': '767032-B21', 'typeID': 'ISS', 'description': 'HPE DL380 Gen9 24SFF CTO Server', 'quantity': 1.0, 'totalQuantity': '=$E$2*E3', 'unitsNetListPrice': 15100.0, 'discount': '=H2', 'unitsNetPrice': '=G3*H3', 'totalPrice': '=I3*F3', 'remarks': '', 'colorTag': 'general'},
	 {'ID': '', 'BOM': '小计', 'typeID': '', 'description': '=C3', 'quantity': '', 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '', 'unitsNetPrice': '', 'totalPrice': '=SUM(J3:J19)', 'remarks': '', 'colorTag': 'subtotal'},
	 {'ID': 2, 'BOM': 'BTO_1', 'typeID': '', 'description': '', 'quantity': 1.0, 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '=H25', 'unitsNetPrice': '', 'totalPrice': '', 'remarks': '', 'colorTag': 'site'
	  },
	 {'ID': '', 'BOM': '455883-B21', 'typeID': 'ISS', 'description': 'HPE BLc 10G SFP+ SR Transceiver', 'quantity': 4.0, 'totalQuantity': '=$E$21*E23', 'unitsNetListPrice': 2900.0, 'discount': '=H21', 'unitsNetPrice': '=G23*H23', 'totalPrice': '=I23*F23', 'remarks': '', 'colorTag': 'general'},
	 {'ID': '', 'BOM': '小计', 'typeID': '', 'description': '=C22', 'quantity': '', 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '', 'unitsNetPrice': '', 'totalPrice': '=SUM(J22:J23)', 'remarks': '', 'colorTag': 'subtotal'},
	 {'ID': '', 'BOM': '总计', 'typeID': '', 'description': '', 'quantity': '', 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': 1, 'unitsNetPrice': '', 'totalPrice': '=SUM(J2:J24)/2', 'remarks': '', 'colorTag': 'total'}];
	dOutput = {
	"ID":"ID",
	"BOM":"产品编码",
	"typeID":"产品型号",
	"description":"项目名称",
	"quantity":"单套数量",
	"totalQuantity":"总数量",
	"unitsNetListPrice":"目录价",
	"discount":"折扣",
	"unitsNetPrice":"单价",
	"totalPrice":"总价",
	"totalListPrice":"总目录价",
	"remarks":"备注",
} ;
	dbModel.assign(lists,list(dOutput.keys()));
	# dbModel.replaceLists();
	dbModel.getStructure();
	dbModel.insertLists();


	