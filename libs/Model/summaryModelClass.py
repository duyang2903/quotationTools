# !/usr/bin/env python3
#encode:UTF-8
# author：杜阳
# ——————————————作用：summary sheet ----------------
from  framework.includeList import *

class summaryModel(object):
	def assign(self,lists,fields,outputKeys,sheetName):
		self.lists = lists;
		self.fields = fields;
		self.sheetName = sheetName;
		self.outputKeys = outputKeys;
		colOrdinal = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
		              'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
		# 先组合成为dict
		self.dCol = dict(zip(self.outputKeys, colOrdinal));	
		self.getSubtotalIndex();
	
	def getSubtotalIndex (self):
		self.aSite = [];
		self.aSubtotal = [];
		self.aTotal = 0;
		for i , arr in enumerate (self.lists):
			if arr['colorTag'] == 'site':
				self.aSite.append(i);
			elif arr['colorTag'] == 'subtotal':

				self.aSubtotal.append(i);
			elif arr['colorTag'] == 'total':
				self.aTotal = i;
			else:
				continue;

		self.aHeader = 0;
		
		
	# **************获得summary sheet的数组**************
	def getSummaryLists(self):
		summaryList = [];
		# 标题行
		d = self.fields;
		d['colorTag'] = 'header';
		summaryList.append(d);
		# 遍历site行的索引
		for i , s in enumerate(self.aSite):
			map = {};
			map['url'] = 'internal:' + self.sheetName + "!" + self.dCol['description'] + str(s + 1 + 1);
			diff = [i for i in ['BOM','typeID'] if i in self.outputKeys];
			tag = diff[0];			
			map['description'] = self.lists[s][tag];
			
			aDiff = [i for i in ['typeID', 'description'] if i in self.outputKeys];
			tag = aDiff[0];	
			try:
				if getParser("inOutmode","outputMode") == "internal":
					tag = getParser('inOutmode',"summaryTypeID");
			except Exception as data:
				error("修改的字段有问题"+str(data));
			# typeID	
			map["typeID"] = '=' + self.sheetName + "!" + self.dCol[tag] + str (s + 1 + 1);
			# quantity
			if 'quantity' in self.outputKeys:
				map['quantity'] = '=' + self.sheetName + "!" + self.dCol['quantity'] + str(s + 1);
			elif 'totalQuantity' in self.outputKeys:
				map['quantity'] = '=' + self.sheetName + "!" + self.dCol['totalQuantity'] + str(s + 1 + 1);
			else:
				map['quantity'] = 0;
			# unitsNetListPrice
			map["unitsNetPrice"] = '=F' + str (i+2) + '/D' + str(i + 2);
			# totalPrice
			map['totalPrice'] = '=' + self.sheetName + '!' + self.dCol['totalPrice'] + str(self.aSubtotal[i] + 1);
			map['ID'] = i + 1;
			# rate
			map['rate'] = '=F' + str(i + 2 ) + '/' + self.sheetName + '!' + self.dCol['totalPrice'] + str (self.aTotal + 1 );
			map['colorTag'] = 'general';
			summaryList.append(map);
		
		# 最后一行
		dict = {};
		for f in self.fields.keys():
			dict [f] = "";
		
		dict['colorTag'] = 'subtotal';
		dict['totalPrice'] = '=SUM(F2:' + 'F' + str(len(summaryList)- 1 + 1) + ")";
		summaryList.append(dict);
		
		
			
		return summaryList;
		
	
	
	




if __name__ == "__main__":
	iSummaryModel =summaryModel();
	lists = [
	{'ID': 'ID', 'BOM': '产品编码', 'typeID': '产品型号', 'description': '项目名称', 'quantity': '单套数量', 'totalQuantity': '总数量', 'unitsNetListPrice': '目录价', 'discount': '折扣', 'unitsNetPrice': '单价', 'totalPrice': '总价', 'remarks': '备注', 'colorTag': 'header'},
	 {'ID': 1, 'BOM': 'HP DL380 Gen9 24SFF CTO Server [#1]', 'typeID': '', 'description': '', 'quantity': 1.0, 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '=H25', 'unitsNetPrice': '', 'totalPrice': '', 'remarks': '', 'colorTag': 'site'},
	 {'ID': '', 'BOM': '767032-B21', 'typeID': 'ISS', 'description': 'HPE DL380 Gen9 24SFF CTO Server', 'quantity': 1.0, 'totalQuantity': '=$E$2*E3', 'unitsNetListPrice': 15100.0, 'discount': '=H2', 'unitsNetPrice': '=G3*H3', 'totalPrice': '=I3*F3', 'remarks': '', 'colorTag': 'general'},
	 {'ID': '', 'BOM': '小计', 'typeID': '', 'description': '=C3', 'quantity': '', 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '', 'unitsNetPrice': '', 'totalPrice': '=SUM(J3:J19)', 'remarks': '', 'colorTag': 'subtotal'},
	 {'ID': 2, 'BOM': 'BTO_1', 'typeID': '', 'description': '', 'quantity': 1.0, 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '=H25', 'unitsNetPrice': '', 'totalPrice': '', 'remarks': '', 'colorTag': 'site'},
	 {'ID': '', 'BOM': '455883-B21', 'typeID': 'ISS', 'description': 'HPE BLc 10G SFP+ SR Transceiver', 'quantity': 4.0, 'totalQuantity': '=$E$21*E23', 'unitsNetListPrice': 2900.0, 'discount': '=H21', 'unitsNetPrice': '=G23*H23', 'totalPrice': '=I23*F23', 'remarks': '', 'colorTag': 'general'},
	 {'ID': '', 'BOM': '小计', 'typeID': '', 'description': '=C22', 'quantity': '', 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': '', 'unitsNetPrice': '', 'totalPrice': '=SUM(J22:J23)', 'remarks': '', 'colorTag': 'subtotal'},
	 {'ID': '', 'BOM': '总计', 'typeID': '', 'description': '', 'quantity': '', 'totalQuantity': '', 'unitsNetListPrice': '', 'discount': 1, 'unitsNetPrice': '', 'totalPrice': '=SUM(J2:J24)/2', 'remarks': '', 'colorTag': 'total'}];
	dOutput = {
	"ID":"ID",
	"BOM":"产品编码",
	"typeID":"产品型号",
	"description":"项目名称",
	"quantity":"单套数量",
	"totalQuantity":"总数量",
	"unitsNetListPrice":"目录价",
	"discount":"折扣",
	"unitsNetPrice":"单价",
	"totalPrice":"总价",
	"totalListPrice":"总目录价",
	"remarks":"备注",
} ;
	fields = {
		"ID":"序号",
		"description":"产品型号",
		"typeID":"产品编码",
		"quantity":"数量",
		"unitsNetPrice":"单价",
		"totalPrice":"总价",
		"rate":"占比"
};
	iSummaryModel.assign(lists,fields , list(dOutput.keys()),'价格明细清单');
	summaryList = iSummaryModel.getSummaryLists();
	# iSummaryModel.addFormula();
	debug(summaryList)
	# debug(iFormulaModel.aSite);

	
	
	
	
	
	
	