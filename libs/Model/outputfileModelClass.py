# !/usr/bin/env python3
# encoding:UTF8
# author：杜阳
# ————————————————作用：获得输出的文件的名称——————————————
from  framework.includeList import *
class outputfileModel(object):
	
	def getOutputFile(self,file):
		fileSplitList = file.rpartition('.')[0].split('_');
		pattern = getParser('inOutmode','outputMode');
		suffix = 0;
		if len(fileSplitList[-1]) >= 3 :
			suffix = 1;
		else:
			try :
				suffix = int(fileSplitList[-1]) + 1;
			except Exception as data: 
				print ("文件名不对"%data);
			
		return fileSplitList[0] + "_" + pattern + "_" + str(suffix)+".xls";
		
		
if __name__ == "__main__":
	outputfileModel = outputfileModel();
	print(outputfileModel.getOutputFile("H3CI-31518-00_20170714-英大证券_含标准价.xls"));