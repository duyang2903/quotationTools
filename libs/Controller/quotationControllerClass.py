# !/usr/bin/env python3
#encode:UTF8
from  framework.includeList import  *
from libs.Model.rehandleModelClass import *;
from framework.libs.view.XlsWriterClass import *;

class quotationController(object):

	def index(self):
# —————————————————————————参数准备—————————————————————————
		# 分别获取输入和输出文件的名称
		var = __import__("libs.inputVariable")
		inputvar = getattr(var,"inputVariable")		
		inputFile = M("file").getProjectName();
		outputFile = M("outputfile").getOutputFile(inputFile);
		info("打开的文件是" + inputFile);
		# 获得输入和输出的keys
		[inputParam , outputParam] = M("parameter").getParameter(inputFile);
		# 以quotationTools的根目录作为基准
		basepath = os.path.dirname(os.path.dirname(os.path.dirname(__file__)));
		inputPath = os.path.join(basepath,getParser('path','inputfilePath'),inputFile);
		outputPath = os.path.join(basepath,getParser('path','outputfilePath'),outputFile);
		# 主sheetName
		sheetName = '价格明细清单';
# —————————————————————————获取各种类型的数组—————————————————————————
		# 读取Excel内容
		lists = XlrdTool().getAssociativeArray(inputPath, sheetName, inputParam.keys())
		# 需要加入如果没有读出的情况
		# 重构数组
		diffList = [param for param in outputParam if  param not in inputParam]
		rehandleInstance = M("rehandle");
		rehandleInstance.assign(lists);
		lists = rehandleInstance.doRehandel(diffList);
		# 如果开启了从DB取数功能
		try:
			# if getParser('ajustableParam','fromDB') == "True":
			if getParser('db','fromDB') == "True":
				dbModel = M('DB');
				dbModel.assign(lists, list(outputParam.keys()));
				dbModel.replaceLists();
				# info("使用数据库作为数据源")
		except Exception as data:
			# 如数据库没运行则
			info("使用原始表格中的数据作为数据源");
		# ----------------------价格明细清单Sheet--------------------------	
		# 添加公式
		iFormula = M("formula");
		iFormula.assign(lists, list(outputParam.keys()));
		lists = iFormula.addFormula();
		# 替换首行为想让他输出的模式
		for k in outputParam.keys():
			lists[0][k] = outputParam[k];
		# —————————————————————————summary Sheet————————————————————
		# 获得SummaryList
		iSummaryModel = M('summary');

		summaryFields =  getattr(inputvar,"summaryFields");
		iSummaryModel.assign(lists,summaryFields,list(outputParam.keys()),sheetName);
		summaryList = iSummaryModel.getSummaryLists();	
		# —————————————————————————合并同类项Sheet—————————————————————————
		iMerge = M('merge');
		mergeFields = getattr(inputvar ,'mergeFields');
		if 'typeID' not in outputParam.keys():
			del mergeFields['typeID']
		iMerge.assign(lists,mergeFields );
		mergeLists = iMerge.getMergeLists();
		
		# —————————————————————————不含价格Sheet—————————————————————————
		listmodel = M('list');
		listFields = getattr(inputvar, 'detailFields');
		listmodel.assign(lists,listFields);
		detailLists = listmodel.getPricefree();
		
		hideCols = getattr(inputvar,'hideCols');
		# —————————————————————————PMS Sheet —————————————————————————
		pmsmodel = M("PMS");
		pmsFields = getattr(inputvar , 'pmsFields');
		pmsmodel.assign(lists,pmsFields);
		pmsLists = pmsmodel.getPMS();
# —————————————————————————打印lists为Excel—————————————————————————
		try:
			
			xlswriter = XlsWriter(outputPath);
			# —————————————————————————打印价格明细—————————————————————————
			xlswriter.assign(lists,list(outputParam.keys()),sheetName,"totalConfigformat");
			# xlswriter.setHidden(hideCols);
			xlswriter.display();	
			info("成功打印价格明细")
			# —————————————————————————打印Summary—————————————————————————
			try:
				sFields = list(summaryFields.keys())
				sFields.remove("colorTag")
				xlswriter.assign(summaryList,sFields,'summary',"summaryformat")
				xlswriter.display();
				xlswriter.writeURL()
				info("成功打印summary")
			except Exception as data:
				error("打印summary sheet时出现问题");
			# —————————————————————————打印merge—————————————————————————
			try :
				mf =  getattr(inputvar ,'mergeFields');
				mergeKey = list(mf.keys())
				mergeKey.remove("colorTag")
				xlswriter.assign(mergeLists,mergeKey,'merge',"mergeformat")
				xlswriter.display();
				info("成功打印merge")
			except Exception as data:
				error("打印merge sheet出现问题")
			# ————————打印清单（无价格）——————————
			try:
				xlswriter.assign(detailLists , list(listFields.keys()),'清单（无价格）','listformat');
				xlswriter.display();
				info("成功打印无价格页")
			except Exception as data:	
				error("打印无价格清单出现问题");
			# —————————————————————PMS清单打印——————————————
			try :
				xlswriter.assign(pmsLists,list(pmsFields.keys()),'PMS','pmsformat');
				xlswriter.display();
				info("成功打印PMS页")
			except Exception as data:
				error("打印PMS sheet出现问题")
			
			info("表格刷新成功")
			
		except Exception as data:
			error("文档已经打开，或者不存在，或有不存在的字段:"+str(data))
		finally:
			# 无论如何都需要关闭文件流
			xlswriter.close();
			# 根据设置删除源文件
			if getParser('inOutmode','deleteSource') == 'True':
				if os.path.isfile(inputPath):
					info("删除源文件")
					os.remove(inputPath)
					
	# **************在数据库中添加从Excel里面读取的数据**************		
	def insert (self)	:
		# 分别获取输入和输出文件的名称
		var = __import__("libs.inputVariable")
		inputvar = getattr(var,"inputVariable")		
		inputFile = M("file").getProjectName();
		outputFile = M("outputfile").getOutputFile(inputFile);
		info("打开的文件是" + inputFile);
		# 获得输入和输出的keys
		[inputParam , outputParam] = M("parameter").getParameter(inputFile);
		# 以quotationTools的根目录作为基准
		basepath = os.path.dirname(os.path.dirname(os.path.dirname(__file__)));
		inputPath = os.path.join(basepath,getParser('path','inputfilePath'),inputFile);
		outputPath = os.path.join(basepath,getParser('path','outputfilePath'),outputFile);
		# 主sheetName
		sheetName = '价格明细清单';
# —————————————————————————获取各种类型的数组—————————————————————————
		# 读取Excel内容
		lists = XlrdTool().getAssociativeArray(inputPath, sheetName, inputParam.keys())
		# 重构数组
		diffList = [param for param in outputParam if  param not in inputParam]
		rehandleInstance = M("rehandle");
		rehandleInstance.assign(lists);
		lists = rehandleInstance.doRehandel(diffList);
		# 如果开启了从DB取数功能
		try:
			if getParser('db','fromDB') == "True":
				dbModel = M('DB');
				dbModel.assign(lists, list(outputParam.keys()));
				# dbModel.getStructure();
				dbModel.insertLists();
				# info("使用数据库作为数据源")
		except Exception as data:
			# 如数据库没运行则
			info("使用原始表格中的数据作为数据源");		
		# ----------------------价格明细清单Sheet--------------------------	
		# 添加公式
		iFormula = M("formula");
		iFormula.assign(lists, list(outputParam.keys()));
		lists = iFormula.addFormula();
		# 替换首行为想让他输出的模式
		for k in outputParam.keys():
			lists[0][k] = outputParam[k];	
			xlswriter = XlsWriter(outputPath);
			
		# —————————————————————————打印价格明细—————————————————————————
		try:
			xlswriter.assign(lists,list(outputParam.keys()),sheetName,"totalConfigformat");
			# xlswriter.setHidden(hideCols);
			xlswriter.display();	
			info("成功打印价格明细")	
		except Exception as data:	
			error("打印明细页失败");
		finally:
			# 无论如何都需要关闭文件流
			xlswriter.close();
			# 根据设置删除源文件
			if getParser('inOutmode','deleteSource') == 'True':
				if os.path.isfile(inputPath):
					info("删除源文件")
					os.remove(inputPath)			
		
		