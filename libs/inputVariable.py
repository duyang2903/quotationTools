
# exportInput = {
	# "ID":"ID",
	# "BOM":"产品编码",
	# "typeID":"产品型号",
	# "description":"项目名称",
	# "quantity":"单套数量",
	# "unitsNetListPrice":"目录价",
	# "discount":"折扣",
	# "unitsNetPrice":"单价",
	# "totalPrice":"总价",
	# "totalListPrice":"总目录价",
	# "PL":"产线",
	# "waston":"WATSON_LINE_ITEM_ID",
	# "remarks":"备注"
# }
exportInput = {
	"ID":"ID",
	"BOM":"产品编码",
	"typeID":"产品型号",
	"description":"项目名称",
	"quantity":"单套数量",
	"unitsNetListPrice":"目录价",
	"discount":"折扣",
	"unitsNetPrice":"单价",
	"totalPrice":"总价",
	"totalListPrice":"总目录价",
	"remarks":"备注",
	"PL":"产线"
	# "waston":"WATSON_LINE_ITEM_ID",
}



internalInput = {
	"ID":"ID",
	"BOM":"产品编码",
	"typeID":"产品型号",
	"description":"项目名称",
	"quantity":"单套数量",
	"totalQuantity":"总数量",
	"unitsNetListPrice":"目录价",
	"discount":"折扣",
	"unitsNetPrice":"单价",
	"totalPrice":"总价",
	"remarks":"备注"

}

internalOutput = {
	"ID":"ID",
	"BOM":"产品编码",
	"typeID":"产品型号",
	"description":"项目名称",
	"quantity":"单套数量",
	"totalQuantity":"总数量",
	"unitsNetListPrice":"目录价",
	"discount":"折扣",
	"unitsNetPrice":"单价",
	"totalPrice":"总价",
	"remarks":"备注"

}


clientInput = {
	"ID":"ID",
	"BOM":"产品编码",
	"typeID":"产品型号",
	"description":"项目名称",
	"totalQuantity":"总数量",
	"unitsNetPrice":"单价",
	"totalPrice":"总价",
	"remarks":"备注"

};

clientOutput = {
	"ID":"ID",
	"typeID":"产品型号",
	"description":"项目名称",
	# "quantity":"单套数量",
	"totalQuantity":"总数量",
	"unitsNetPrice":"单价",
	"totalPrice":"总价",
	"remarks":"备注"
}

HPEInput = {
	"ID":"ID",
	"quantity":"单套数量",
	"BOM":"产品编码",
	"description":"项目名称",
	"totalQuantity":"总数量",
	"unitsNetListPrice":"目录价",
	"discount":"折扣",
	"unitsNetPrice":"单价",
	"totalPrice":"总价",
	"remarks":"备注"
};

HPEOutput = {
	"ID":"ID",
	"quantity":"单套数量",
	"BOM":"产品编码",
	"description":"项目名称",
	"totalQuantity":"总数量",
	"unitsNetListPrice":"目录价",
	"discount":"折扣",
	"unitsNetPrice":"单价",
	"totalPrice":"总价",
	"remarks":"备注"
}

pinganInput = {
	"ID":"序号",
	"typeID":"产品名称",
	"description":"详细说明",
	"unitsNetListPrice":"列表价（含税）",
	"discount":"折扣（OFF）",
	"unitsNetPrice":"折后单价（含税）",
	"taxExclusivePrice":"折后未税净价",
	"unit":"单位",
	"totalQuantity":"数量",
	"billType":"发票类型",
	"totalTaxExcPrice":"未含税总价",
	"totalPrice":"含税总价",
	"taxRate":"税率",
	"total":"小计（元）",
	"remarks":"备注"
}

pinganOutput = {
	"ID":"序号",
	"typeID":"产品名称",
	"description":"详细说明",
	"unitsNetListPrice":"列表价（含税）",
	"discount":"折扣（OFF）",
	"unitsNetPrice":"折后单价（含税）",
	"taxExclusivePrice":"折后未税净价",
	"unit":"单位",
	"totalQuantity":"数量",
	"billType":"发票类型",
	"totalTaxExcPrice":"未含税总价",
	"totalPrice":"含税总价",
	"taxRate":"税率",
	"total":"小计（元）",
	"remarks":"备注"
}


hideCols = ["totalQuantity"]

diff = {
	'totalNum':'0',
	"unit":"个",
	"billType":"增值税",
	"taxRate":"17%"


};


mReplace = {
	"SFP-XG-SX-MM850-E":'SFP-XG-SX-MM850',
	"SFP-XG-SX-MM850-D":'SFP-XG-SX-MM850'

};


mRemarks={
	"SFP-XG-SX-MM850-E":'万兆光模块',
	"SFP-XG-SX-MM850-D":'万兆光模块'
	
}


summaryFields = {
	"ID":"序号",
	"description":"产品型号",
	"typeID":"产品编码",
	"quantity":"数量",
	"unitsNetPrice":"单价",
	"totalPrice":"总价",
	"rate":"占比"
};

mergeFields = {
	"ID":"序号",
	"BOM":"产品编码",
	"typeID":"产品编码",
	"description":"产品型号",
	"totalQuantity":"数量",
	"unitsNetListPrice":"目录单价",
	"discount":"折扣",
	"unitsNetPrice":"单价",
	"totalPrice":"总价",
	"rate":"占比"
};



dbConfig = {
	"host":"127.0.0.1",
	"port":"3306",
	"db":"quotation",
	"user":"root",
	"passwd":"root",
	"charset":"utf8"
};


# detailFields = {
	# "ID":"序号",
	# "typeID":"产品编码",
	# "description":"产品型号",
	# "totalQuantity":"数量",
	# "remarks":"备注"
# }

detailFields = {
	"ID":"序号",
	"BOM":"PN码",
	"typeID":"产品编码",
	"description":"产品型号",
	"quantity":"数量",
	"remarks":"备注"
}

pmsFields = {
	"siteNames":"组合名称",
	"encode":"编码",
	"name":"名称",
	"BOM":"BOM编码",
	"typeID":"名称",
	"description":"描述",
	"form":"类型",
	"unitsNetListPrice":"单价",
	"discount1":"折扣",
	"discount2":"折扣",
	"discount3":"折扣",
	"discount4":"折扣",
	"quantity":"数量"

};





