
本工具要的功能是**对NHCT（新华三在线配置系统）导出来的表格进行后处理**，增加更加丰富的统计功能等。

比如说我们最常用的明细页

![image.png](http://upload-images.jianshu.io/upload_images/1323506-85356d7b5534a0fa.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 去掉页面上大量的冗余信息，我们只关注最关键的部分。

- 加上**单套数量。**NHCT导出来的格式只有总数量，但是我们要核对的是单套设备的数量，非常不灵活。

- 可以批量修改折扣

对于每份配置表格，我们至少有50%时间在处理的表格，比如调公式、改颜色、甚至还要按照标书的要求进行行列的变换，最最关键的是，还需要**检查一下公式是否错误**，报错了一个价格，可是我们自己背锅啊……

但是这一部分工作都是很固定的，完全可以使用一个自动化工具来完成，这也是我自己动手做这个自动化脚本的初衷

# 这个脚本能带来的价值

那这个脚本可以做到哪些**改变**呢？

- 对明细页，加上了单套数量列，可以批量修改折扣

- 加上一个导航页，可以快速索引到价格明细页

![image.png](http://upload-images.jianshu.io/upload_images/1323506-8ad5bcaaf9022df9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 可以自动生成PMS（销售管理系统）上传模板，方便我们**下单**
![image.png](http://upload-images.jianshu.io/upload_images/1323506-3e78c9ad5f5ef287.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 比如说我们想看**光模块在总的价格中的占比**，这个脚本也能帮你实现。

![image.png](http://upload-images.jianshu.io/upload_images/1323506-026c92297d514645.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

如果进行了修改，我们只需要**重新运行一下**就可以了，不用那么费力的再去调公式。

总的来说这个自动化小工具可以

- 提供工作效率50%以上，我们可以把时间花在更多更有价值的事上

- 可以对每一个Site或者散件进行占比分析，辅助我们决策

- 表格修改以后，只需再次运行即可，避免低级错误

![image.png](http://upload-images.jianshu.io/upload_images/1323506-6f1e1046c52b00e6.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

那么有的同事会问了，这样一个工具是不是部署和使用起来特别麻烦呢？

其实不然，下面我们会讲一下使用和安装的方法，如果不太明白的还有Gif动画哦。

# 使用方法

## 安装部署

安装只需要三步

- 下载并安装Python环境（放心，只需要点下一步下一步即可）
![image.png](http://upload-images.jianshu.io/upload_images/1323506-d687acd8c51ba14b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

**特别要注意，需要勾上Add Python 3.6 to PATH，然后点“Install Now”即可完成安装。**

- 下载源码：[下载地址]( "https://pan.baidu.com/s/1i6yXtIH")

- 执行一个脚本（注意要切换到**因特网**）

    进入代码里面install目录，直接双击install.bat
![image.png](http://upload-images.jianshu.io/upload_images/1323506-0608d87f08fa9d9e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

然后就安装完毕了……


## 如何使用

接着就是如何使用的问题了。

我将代码分为了两套，他们的区别在于格式不同。

H3C分支主要针对于服务器，H3C分支主要针对与网络设备。

![UTOOLS1573789034356.png](http://yanxuan.nosdn.127.net/2bb00a232cc15588d9b10327e91e8456.png)


对于原杭州华三的同事，可能习惯像这样配置。

如下图所示，一个site里面会嵌套若干的设备，输出的文档格式很不规范，程序不好规律，所以需要我们手动进行一些调整。

![UTOOLS1573788852959.png](http://yanxuan.nosdn.127.net/90284e485e5df59eb1e2d9f2328bc416.png)

主要是：

- 删除灰色和、浅蓝色的行

- 清除序号列的内容

- 从“产品型号”列筛选出“空白”行，然后这些行的“序号”列加上数字“1”


![excel.gif](http://upload-images.jianshu.io/upload_images/1323506-5f85a94b44c3d70d.gif?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

然后就将这个Excel文档放入quotationTool目录下（根目录下只能有一个Excel文档）

双击admin.py即可运行。

在project找到输出后的文档

![HPE.gif](http://upload-images.jianshu.io/upload_images/1323506-1edc9d2be74d1df1.gif?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)



生成后的效果


![image.png](http://upload-images.jianshu.io/upload_images/1323506-7e07594cbe3e4485.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

导航页：

![image.png](http://upload-images.jianshu.io/upload_images/1323506-8ad5bcaaf9022df9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

merge页主要是将散件合并同类项，大家可以方便的在里面查看**光模块**占比有多大等。

![image.png](http://upload-images.jianshu.io/upload_images/1323506-0b4557763349c7b0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

PMS下单页：
![image.png](http://upload-images.jianshu.io/upload_images/1323506-469266ee13c9553b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

注意：

- 不能**删除任何列**

- 根目录下只能有一个Excel文档

- 建议不要修改文件名称

- 只能针对NHCT导出来的文档以及根据它运行出来的文档进行处理

# 详细信息

上面只是简单的介绍了一下。

详细的文档可以看

想知道有什么功能，安装部署可参考

- [QuotationTool能做什么.](https://www.jianshu.com/p/04044027503a)

- [QuotationTools自动化脚本的部署和使用](https://www.jianshu.com/p/0b4d02138047)


还想知道代码怎么实现的可以参考：

首先可以了解一下整体的设计思想，主要是MVC设计模式：[【QuotationTool的代码实现】总体代码结构](https://www.jianshu.com/p/9875a0cb25e1)

然后就是项目的通用部分，比如配置文件的解析，log的打印等：[【QuotationTool的代码实现】framework部分](https://www.jianshu.com/p/4067963f1f18)

还需要关注本项目所设计的数据结构：[【QuotationTool】主要数据结构](https://www.jianshu.com/p/44f0a73dd02f)

然后就是主要功能模块了：

- [【QuotationTool】Model的实现（一），获得Excel路径以及Excel输出格式](https://www.jianshu.com/p/d3f4228d8471)
- [【QuotationTool】Model的实现（二），形成价格明细清单.md](https://www.jianshu.com/p/e56f235727fd)
- [【QuotationTool】Model的实现（三），导航页](https://www.jianshu.com/p/aab0beaa30f1)
- [【QuotationTool】Model的实现（四），汇总页.md](https://www.jianshu.com/p/045be4ec94f6)


最后是如何打印Excel的问题

[【QuotationTool】View的实现，输出Excel](https://www.jianshu.com/p/ffc32f2c6888)
